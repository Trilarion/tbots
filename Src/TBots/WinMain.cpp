//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: WinMain.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 16/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "TBots.h"

#ifdef _WIN32
#include <windows.h>
#endif // _WIN32

#ifdef _LINUX
#include <stdio.h>
#endif // _LINUX

//--------------------------------------------------------------------------------------------
#ifdef _WIN32
int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
#endif // _WIN32
#ifdef _LINUX
int main( int argc, char** argv )
#endif // _LINUX
{
#ifdef _LINUX
	HINSTANCE	hInstance = 0;
#endif // _LINUX

	KTBots	TBots;

	//
	// Init
	//
	if( KR_FAILED( TBots.Init( hInstance ) ) )
	{
		KERROR( "TBots.Init failed!" );
		TBots.End();
		return -1;
	}

	//
	// Run
	//
	if( KR_FAILED( TBots.Run() ) )
	{
		KERROR( "TBots.Run failed!" );
		return -1;
	}

	//
	// End
	//
	if( KR_FAILED( TBots.End() ) )
	{
		KERROR( "TBots.End failed!" );
		return -1;
	}

	return 0;
}
