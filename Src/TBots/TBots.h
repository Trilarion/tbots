//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: TBots.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 16/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__TBOTS_H__
#define	__TBOTS_H__

#include "Types.h"
#include "Render.h"
#include "Camera.h"
#include "Sky.h"
#include "Level.h"
#include "Bot.h"
#include "Timer.h"

//--------------------------------------------------------------------------------------------
#define	APP_NAME			"T-Bots"
#define APP_WIDTH			800
#define APP_HEIGHT			600
#define	WINDOW_CLASSNAME	"The TBots Window"

#define	WM_RESTARTGAME		(WM_APP+0)
#define	WM_RESTARTBATTLE	(WM_APP+1)
#define	WM_STEPBYSTEP		(WM_APP+3)
#define	WM_STEPGO			(WM_APP+4)
#define	WM_TOGGLE3DMODE		(WM_APP+5)
#define	WM_ABOUT			(WM_APP+6)

#ifdef _LINUX
typedef int*	HMODULE;
typedef int*	HINSTANCE;
typedef void*	FARPROC;

#define VK_UP		0
#define VK_DOWN		0
#define VK_LEFT		0
#define VK_RIGHT	0
#define VK_RSHIFT	0
#define VK_RCONTROL	0
#define VK_DELETE	0
#define VK_NEXT		0
#define VK_HOME		0
#define VK_END		0

#define IDNO		0
#define IDYES		1

#endif // _LINUX

//--------------------------------------------------------------------------------------------
class KDllBotInfo
{
public:
	char*		m_pDllName;
	char*		m_pBotName;
	bool		m_bFight;
#ifdef _WIN32
	HMODULE		m_hModule;
#endif // _WIN32
#ifdef _LINUX
	void*		m_hModule;
#endif // _LINUX
	KBot*		m_pBot;
	FARPROC		m_pInfosProc;
	FARPROC		m_pStartGameProc;
	FARPROC		m_pEndGameProc;
	FARPROC		m_pBotActionProc;
	FARPROC		m_pDebugDisplayProc;
	s32			m_BotNumber;
	void*		m_pContext;

	KDllBotInfo()
	{
		m_hModule			= NULL;
		m_pDllName			= NULL;
		m_pBotName			= NULL;
		m_bFight			= false;
		m_pBot				= NULL;
		m_pInfosProc		= NULL;
		m_pBotActionProc	= NULL;
		m_pStartGameProc	= NULL;
		m_pEndGameProc		= NULL;
		m_pDebugDisplayProc	= NULL;
		m_BotNumber			= 0;
		m_pContext			= NULL;
	}

	~KDllBotInfo()
	{
		if( m_pDllName )
			Freep( m_pDllName );

		if( m_pBotName )
			Freep( m_pBotName );
	}

	void	SetDllName( char* pName )
	{
		if( m_pDllName )
			Freep( m_pDllName );

		if( pName )
			m_pDllName = strdup( pName );
	}

	void	SetBotName( char* pName )
	{
		if( m_pBotName )
			Freep( m_pBotName );

		if( pName )
			m_pBotName = strdup( pName );
	}

};

//--------------------------------------------------------------------------------------------
class KTBots
{
protected:
	HWND							m_hWnd;
	HDC								m_hDC;
	bool							m_bQuit;
	KRender*						m_pRender;
	KLevel*							m_pLevel;
	KSky*							m_pSky;
	HINSTANCE						m_hInstance;
	KList<KDllBotInfo*>				m_DllBotInfos;
	KDllBotInfo*					m_pCurrentBot;
	KTIME							m_ActionEndTime;
	char*							m_pStatusMessage;
	s32								m_nRemainingActions;
	bool							m_bStepByStep;
	bool							m_bStepGo;
	KBot*							m_pWinner;
	s32								m_RespawnCountDown;
	KList<char*>					m_MazeList;
	char*							m_pMazeName;
	KCamera							m_Camera;
	bool							m_b3DMode;
	KTIME							m_LastTime;
	s32								m_nFrames;
	s32								m_nFPS;

#ifdef _WIN32
	static LRESULT CALLBACK			WindowProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam );
	static INT_PTR CALLBACK			DialogBotsListProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam );
	static INT_PTR CALLBACK			DialogAboutProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam );
	KRESULT							LoadDLLBots();
#endif // _WIN32									
#ifdef _LINUX
	KRESULT							LoadSOBots();
#endif // _LINUX

	KRESULT							MoveFrame();
	KRESULT							DrawFrame();
//	KRESULT							DrawDebugBots();
	KRESULT							DrawInterface();
	KRESULT							ManageGame();

	KRESULT							SpawnBots();
	KRESULT							SpawnBonus( u32 nBonus );
	KRESULT							SpawnRegenerator( u32 nRegenerator );
	KRESULT							SpawnMedikit( u32 nMedikit );
	KRESULT							SpawnBinoculars( u32 nBinoculars );
	KRESULT							SpawnMine( u32 nMine );
	KRESULT							SpawnLaser( u32 nLaser );
	KRESULT							NextTurn();

	KRESULT							StartActionNothing( KDllBotInfo* pDllBotInfo );
	KRESULT							StartActionMove( KDllBotInfo* pDllBotInfo, KBOTMOVEDIR MoveDir );
	KRESULT							StartActionAttack( KDllBotInfo* pDllBotInfo, KBOTATTACKTARGET Target );
	KRESULT							StartActionAttackLaser( KDllBotInfo* pDllBotInfo, KBOTATTACKTARGET Target );
	KRESULT							StartActionDrop( KDllBotInfo* pDllBotInfo, KITEMTYPE ItemType );

	void							Hurt( KPt Pos, s32 Life );

	KRESULT							SetpMessage( char* pMessage );

	s32								GetnAliveBots();
	s32								GetnItems( KITEMTYPE Type );

	void							SpawnItems();
	void							StartGame();
	void							EndGame();
	KRESULT							RestartGame();
	KRESULT							RestartBattle();
	KRESULT							LoadGame();
	KRESULT							UnloadGame();

	void							SetpMazeName( char* pMazeName );
										
public:								
	KTBots();						
	~KTBots();						

	KRESULT							Init( HINSTANCE hInstance );
	KRESULT							Run();
	KRESULT							End();
};

#endif // __TBOTS_H__
