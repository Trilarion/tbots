// EditorView.h : interface of the CEditorView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_EDITORVIEW_H__05CEFDBC_7F15_423F_B972_05817B8DFF0C__INCLUDED_)
#define AFX_EDITORVIEW_H__05CEFDBC_7F15_423F_B972_05817B8DFF0C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


////////////////////////////////////////////////////////////////////////////////
// Defines & Enumerations
////////////////////////////////////////////////////////////////////////////////
#define COLOR_CELL_LOCKED		KRGBA(0, 255, 0, 128)
#define COLOR_CELL_FLIPH		KRGBA(255, 0, 0, 128)
#define COLOR_CELL_FLIPV		KRGBA(255, 0, 0, 128)


class KRender;
class KLevel;
class CEditorDoc;

class CEditorView : public CView
{
protected: // create from serialization only
	CEditorView();
	DECLARE_DYNCREATE(CEditorView)

	KRender		*m_pRender;
	KLevel		*m_pLevel;
	CellType	m_CellToFill;

// Attributes
public:
	CEditorDoc* GetDocument();

	void DrawFrame();
	void DrawLevel();
	void InitRender();
	void NewLevel(u32 _Width, u32 _Height);
	void PaintLevel(u32 _x, u32 _y, bool _bErase, bool _bClick);
	void FillLevel(u32 _x, u32 _y, CellType Brush);
	void ClearLevel();


// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditorView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CEditorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CEditorView)
	afx_msg void OnDestroy();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in EditorView.cpp
inline CEditorDoc* CEditorView::GetDocument()
   { return (CEditorDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITORVIEW_H__05CEFDBC_7F15_423F_B972_05817B8DFF0C__INCLUDED_)
