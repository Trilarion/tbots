// TilesLibraryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "editor.h"

#include "..\Common\Level.h"

#include "TilesLibraryDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//==============================================================================
//
// 							CLASS CTilesLibraryDlg
//
//==============================================================================

CTilesLibraryDlg::CTilesLibraryDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTilesLibraryDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTilesLibraryDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_pRender	= NULL;
	m_pShader	= NULL;
}

CTilesLibraryDlg::~CTilesLibraryDlg()
{
	SafeDelete(m_pShader);
	SafeDelete(m_pRender);
}

////////////////////////////////////////////////////////////////////////////////
// DoDataExchange
//															   CTilesLibraryDlg
////////////////////////////////////////////////////////////////////////////////
void CTilesLibraryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTilesLibraryDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTilesLibraryDlg, CDialog)
	//{{AFX_MSG_MAP(CTilesLibraryDlg)
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


////////////////////////////////////////////////////////////////////////////////
// OnPaint
//															   CTilesLibraryDlg
////////////////////////////////////////////////////////////////////////////////
void CTilesLibraryDlg::OnPaint() 
{
	CPaintDC dc1(this); // device context for painting
	CPaintDC dc2(GetDlgItem(IDC_STATIC_LIBRARY_VIEW)); // device context for painting
	
	DrawFrame();
}

////////////////////////////////////////////////////////////////////////////////
// OnDestroy
//															   CTilesLibraryDlg
////////////////////////////////////////////////////////////////////////////////
void CTilesLibraryDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	if (m_pRender)
		m_pRender->End();		
}

////////////////////////////////////////////////////////////////////////////////
// Init
//															   CTilesLibraryDlg
////////////////////////////////////////////////////////////////////////////////
void CTilesLibraryDlg::Init()
{
	KASSERT(!m_pRender);

	CRect	Rect;
		
	GetDlgItem(IDC_STATIC_LIBRARY_VIEW)->GetClientRect(&Rect);

	HWND	hWnd = GetDlgItem(IDC_STATIC_LIBRARY_VIEW)->m_hWnd;
	m_pRender = new KRender();
	m_pRender->Init( hWnd, ::GetDC( hWnd ), Rect.right - Rect.left, Rect.bottom - Rect.top, 32, false );

	m_pRender->LoadShader("Tiles.png", &m_pShader);
}

////////////////////////////////////////////////////////////////////////////////
// DrawFrame
//															   CTilesLibraryDlg
////////////////////////////////////////////////////////////////////////////////
void CTilesLibraryDlg::DrawFrame()
{
	if (m_pRender)
	{
		m_pRender->BeginScene();

		DrawLibrary();

		m_pRender->EndScene();
		m_pRender->Flip();
	}
}

////////////////////////////////////////////////////////////////////////////////
// DrawLibrary
//															   CTilesLibraryDlg
////////////////////////////////////////////////////////////////////////////////
void CTilesLibraryDlg::DrawLibrary()
{
	float	pTU[4];
	float	pTV[4];
	u8 Width = m_pShader->GetShaderInfo().m_Width / CELL_SIZE;

	pTU[0] = 0.0f;	pTV[0] = 0.0f;
	pTU[1] = 1.0f;	pTV[1] = 0.0f;
	pTU[2] = 0.0f;	pTV[2] = 1.0f;
	pTU[3] = 1.0f;	pTV[3] = 1.0f;

	m_pRender->DrawQuad( 0, 0, m_pShader->GetShaderInfo().m_Width, m_pShader->GetShaderInfo().m_Height, m_pShader, KRGBA(255, 255, 255, 255), KRGBA(0, 0, 0, 0), KRM_NORMAL, pTU, pTV );

	m_pRender->DrawQuad( (gTBotsEditorApp.GetCurrentBrush() % Width) * CELL_SIZE,
						 (gTBotsEditorApp.GetCurrentBrush() / Width) * CELL_SIZE,
						 CELL_SIZE, CELL_SIZE,
						 0,
						 KRGBA(255, 0, 0, 128) );
}

void CTilesLibraryDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	u32 x = point.x / CELL_SIZE;
	u32 y = point.y / CELL_SIZE;
	u8 Width = m_pShader->GetShaderInfo().m_Width / CELL_SIZE;

	gTBotsEditorApp.SetCurrentBrush(x+ y*Width);

	DrawFrame();
	
	CDialog::OnLButtonDown(nFlags, point);
}
