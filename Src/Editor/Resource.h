//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Editor.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_MAZETYPE                    129
#define IDD_SETTINGS                    130
#define IDD_NEWMAZE                     131
#define IDB_BITMAP1                     133
#define IDB_TOOLS                       133
#define IDD_TILES_LIBRARY               134
#define IDC_EDIT_MAZE_NAME              1000
#define IDC_EDIT_MAZE_AUTHOR            1001
#define IDC_EDIT_MAZE_WIDTH             1002
#define IDC_EDIT_MAZE_HEIGHT            1003
#define IDC_STATIC_LIBRARY_VIEW         1004
#define IDC_BUTTON_LOAD_LIBRARY         1005
#define ID_EDITION_SETTINGS             32771
#define ID_BUTTON_DRAW                  32772
#define ID_BUTTON_FILL                  32773
#define ID_BUTTON_PICKER                32774
#define ID_BUTTON_FLIPH                 32775
#define ID_BUTTON32776                  32776
#define ID_BUTTON_FLIPV                 32777
#define ID_BUTTON_LOCK                  32778
#define ID_BUTTON_ITEM_MODE             32781
#define ID_BUTTON_CLEAR                 32782

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32783
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
