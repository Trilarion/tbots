// EditorDoc.h : interface of the CEditorDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_EDITORDOC_H__E9F457EB_8309_4D67_9F51_0A404C0DF031__INCLUDED_)
#define AFX_EDITORDOC_H__E9F457EB_8309_4D67_9F51_0A404C0DF031__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "..\Common\Level.h"


class CEditorView;


class CEditorDoc : public CDocument
{
protected: // create from serialization only
	CEditorDoc();
	DECLARE_DYNCREATE(CEditorDoc)

// Attributes
public:

// Operations
public:

	CEditorView *GetView();

	u8 GetMazeWidth()				{ return m_MazeHeader.Width;  }
	u8 GetMazeHeight()				{ return m_MazeHeader.Height; }
	CellType GetMazeCell(u32 _x, u32 _y)
	{
		if (!m_MazeCells)
			return 0;
		
		if ( (_x >= m_MazeHeader.Width) || (_y >= m_MazeHeader.Height) )
			return 0;
		
		return m_MazeCells[m_MazeHeader.Width*_y + _x];
	}

protected:
	KTBotLevelHeader m_MazeHeader;
	CellType		 *m_MazeCells;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditorDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CEditorDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CEditorDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITORDOC_H__E9F457EB_8309_4D67_9F51_0A404C0DF031__INCLUDED_)
