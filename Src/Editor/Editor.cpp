// Editor.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Editor.h"

#include "MainFrm.h"
#include "ChildFrm.h"
#include "EditorDoc.h"
#include "EditorView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



////////////////////////////////////////////////////////////////////////////////
// Global variables
////////////////////////////////////////////////////////////////////////////////

CEditorApp gTBotsEditorApp;

CImageList gImageListTools;


//==============================================================================
//
// 								CLASS CAboutDlg
//
//==============================================================================
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CEditorApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}


//==============================================================================
//
// 								CLASS CEditorApp
//
//==============================================================================


BEGIN_MESSAGE_MAP(CEditorApp, CWinApp)
	//{{AFX_MSG_MAP(CEditorApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_EDITION_SETTINGS, OnEditionSettings)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()


////////////////////////////////////////////////////////////////////////////////
// CEditorApp constructor
// 																	 CEditorApp
////////////////////////////////////////////////////////////////////////////////
CEditorApp::CEditorApp()
{
	m_CurrentBrush = 0;
	m_CurrentTool	= EditorToolDraw;
	m_OldTool		= m_CurrentTool;
	m_CellFlags		= 0;
}

////////////////////////////////////////////////////////////////////////////////
// InitInstance
// 																	 CEditorApp
////////////////////////////////////////////////////////////////////////////////
BOOL CEditorApp::InitInstance()
{
	AfxEnableControlContainer();

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
//	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	if (!AfxOleInit())
		AfxMessageBox("Error OleInit");

	SetRegistryKey(_T("T-Bots Team"));

	LoadStdProfileSettings(5);

	Init();

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Register document templates
	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate (
						IDR_MAZETYPE,
						RUNTIME_CLASS(CEditorDoc),
						RUNTIME_CLASS(CChildFrame), // custom MDI child frame
						RUNTIME_CLASS(CEditorView)
					);

	AddDocTemplate(pDocTemplate);

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;

	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return false;

	m_pMainWnd = pMainFrame;

	// Enable drag/drop open
	m_pMainWnd->DragAcceptFiles();

	// Enable DDE Execute open
	EnableShellOpen();
	RegisterShellFileTypes(true);

	// Dispatch commands specified on the command line
//	if (!ProcessShellCommand(cmdInfo))
//		return false;

	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();


	return true;
}

////////////////////////////////////////////////////////////////////////////////
// ExitInstance
// 																	 CEditorApp
////////////////////////////////////////////////////////////////////////////////
int CEditorApp::ExitInstance() 
{
	Release();

	return CWinApp::ExitInstance();
}

////////////////////////////////////////////////////////////////////////////////
// Init
// 																	 CEditorApp
////////////////////////////////////////////////////////////////////////////////
void CEditorApp::Init()
{
	gImageListTools.Create(IDB_TOOLS,32,1,RGB(255,0,255));
}

////////////////////////////////////////////////////////////////////////////////
// Release
// 																	 CEditorApp
////////////////////////////////////////////////////////////////////////////////
void CEditorApp::Release()
{
}

////////////////////////////////////////////////////////////////////////////////
// OnEditionSettings
// 																	 CEditorApp
////////////////////////////////////////////////////////////////////////////////
void CEditorApp::OnEditionSettings() 
{
	
}

BOOL CEditorApp::OnIdle(LONG lCount) 
{
	if( !m_pMainWnd->IsIconic() )
	{
		if (m_pMainWnd)
		{
			CEditorView *view = (CEditorView *)((CMainFrame *)m_pMainWnd)->GetActiveFrame()->GetActiveView();

			if (view)
				view->DrawFrame();
		}
	}
	else
		Sleep( 100 );

	CWinApp::OnIdle(lCount);

	return TRUE;
}
