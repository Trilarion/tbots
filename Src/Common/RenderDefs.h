//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: RenderDefs.h
//	Author			: - Dino			dino@zythum-project.com
//
//	Date			: 26/10/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__RENDERDEFS_H__
#define	__RENDERDEFS_H__

#include "Types.h"
#include "Matrix.h"

class KCamera;
class KMesh;
class KShader;
class KMesh;

//--------------------------------------------------------------------------------------------
typedef u16							KINDEX;

//--------------------------------------------------------------------------------------------
typedef enum _KRMODE
{
	KRM_NORMAL,
	KRM_ADD,
	KRM_SUBSTRACT
} KRMODE;
									
//--------------------------------------------------------------------------------------------
class KTLVertex
{
public:
	float	x;
	float	y;
	float	z;
	float	rhw;
	KCOLOR	color;
	KCOLOR	specular;
	float	tu;
	float	tv;

	inline KTLVertex()
	{
		x = 0.0f;
		y = 0.0f;
		z = 0.0f;
		rhw = 0.0f;
		color = 0;
		specular = 0;
		tu = 0.0f;
		tv = 0.0f;
	}
	
	inline KTLVertex( float x, float y, float z, float rhw, KCOLOR color, KCOLOR specular = 0, float tu = 0.0f, float tv = 0.0f)
	{
		this->x	= x;
		this->y	= y;
		this->z	= z;
		this->rhw = rhw;
		this->color = color;
		this->specular = specular;
		this->tu = tu;
		this->tv = tv;
	}
};

//--------------------------------------------------------------------------------------------
class KVertex
{
public:
	float	x;
	float	y;
	float	z;
	float	nx;
	float	ny;
	float	nz;
	float	tu;
	float	tv;

	inline KVertex()
	{
	}
};

//--------------------------------------------------------------------------------------------------------------------------
class KRenderFace
{
public:
	void*					m_pVertex;
	u16						m_nVertex;
	KINDEX*					m_pIndices;
	u16						m_nIndices;

	KRenderFace()
	{
		m_pVertex		= NULL;
		m_pIndices		= NULL;
		m_nVertex		= 0;
		m_nIndices		= 0;
	}
};

#endif // __RENDERDEFS_H__
