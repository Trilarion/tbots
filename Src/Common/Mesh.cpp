//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Mesh.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 05/08/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "Render.h"
#include "Shader.h"
#include "Mesh.h"

#define	KMSHVERSION		1

//--------------------------------------------------------------------------------------------
KMesh::KMesh()
{
	m_pRender		= NULL;
	m_pMaterials	= NULL;
	m_nMaterials	= 0;
}

//--------------------------------------------------------------------------------------------
KMesh::~KMesh()
{
	Free();
}

//--------------------------------------------------------------------------------------------
KRESULT KMesh::LoadMesh( KRender* pRender, char* pFileName, bool bLoadShader )
{
	char	pPath[1024];

	m_pRender = pRender;

	strcpy( pPath, pFileName );
	strcat( pPath, ".msh" );
	if( !LoadMSH( pPath, bLoadShader ) )
	{
		strcpy( pPath, pFileName );
		strcat( pPath, ".ase" );
		if( !LoadASE( pPath, bLoadShader ) )
		{
			KERROR( "Cannot load mesh %s\n", pFileName );
			return KR_ERROR;
		}

		strcpy( pPath, pFileName );
		strcat( pPath, ".msh" );
		SaveMSH( pPath );
	}

	return KR_OK;
}

//--------------------------------------------------------------------------------------------
void KMesh::Free()
{
	SafeDeleteArray( m_pMaterials );

	for( u32 i = 0; i < m_SubMeshes.size(); i ++ )
	{
		SafeDelete( m_SubMeshes[i] );
	}
	m_SubMeshes.clear();
}

//--------------------------------------------------------------------------------------------
void KMesh::DrawSubMesh( u32 SubMeshId )
{
	if( SubMeshId >= m_SubMeshes.size() )
		return;

	KSubMesh*	pSubMesh;
	KRenderFace	Face;

	pSubMesh = m_SubMeshes[SubMeshId];

	Face.m_pVertex	= pSubMesh->m_pVertex;
	Face.m_nVertex	= pSubMesh->m_nVertex;

	for( u32 i = 0; i < pSubMesh->m_nFaceMeshes; i ++ )
	{
		Face.m_pIndices	= pSubMesh->m_pFaceMeshes[i].m_pIndex;
		Face.m_nIndices	= pSubMesh->m_pFaceMeshes[i].m_nIndex;

//		m_pRender->ActivateWireFrame( true );
		if( pSubMesh->m_pFaceMeshes[i].m_MaterialId < m_nMaterials )
			m_pRender->SetShader( m_pMaterials[pSubMesh->m_pFaceMeshes[i].m_MaterialId].m_pShader );
		else
			m_pRender->SetShader( NULL );

		m_pRender->DrawTriangles( Face );
		m_pRender->FlushTriangles();
		m_pRender->ActivateWireFrame( false );
	}
}

//--------------------------------------------------------------------------------------------------------------------------------
#define	FINDTOKEN				m_Parser.FindToken
#define	TOKEN					m_Parser.GetToken
#define	SEARCHTOKEN( __token )	m_Parser.SearchToken( __token )
#define	ISTOKEN( __token )		(stricmp( m_Parser.GetToken(), __token ) == 0)

//--------------------------------------------------------------------------------------------
bool KMesh::LoadASE( char* pFileName, bool bLoadShader )
{
	if( !m_Parser.LoadASE( pFileName ) )
		return false;
	
	FINDTOKEN();
	if( !ISTOKEN( "*3DSMAX_ASCIIEXPORT" ) )
	{
		m_Parser.Clean();
		return false;
	}

	// *MATERIAL_LIST
	SEARCHTOKEN( "*MATERIAL_LIST" );
	SEARCHTOKEN( "*MATERIAL_COUNT" );
	FINDTOKEN();

	m_nMaterials = atoi( TOKEN() );
	m_pMaterials = new KMaterialMesh[m_nMaterials];

	// Load materials
	for( u32 i = 0; i < m_nMaterials; i ++ )
	{
		SEARCHTOKEN( "*MATERIAL" );
		FINDTOKEN();

		SEARCHTOKEN( "*BITMAP" );
		FINDTOKEN();

		u32	k = 0;
		for( u32 j = 0; j < strlen( TOKEN() ); j ++ )
		{
			if( ( TOKEN()[j] == '/' ) || ( TOKEN()[j] == '\\' ) )
			{
				k = j;
			}
		}
		char	pPath[1024];

		strcpy( pPath, "Textures/" );
		strcat( pPath, &TOKEN()[k + 1] );
		pPath[strlen( pPath ) - 1] = 0;
		m_pMaterials[i].m_pFileName = strdup( pPath );

		if( bLoadShader )
		{
			m_pRender->LoadShader( m_pMaterials[i].m_pFileName, &m_pMaterials[i].m_pShader );
			m_pMaterials[i].m_bShaderAllocated = true;
		}
	}

	// *GEOMOBJECT
	while( SEARCHTOKEN( "*GEOMOBJECT" ) )
	{
		u32			nFaces = 0;
		KSubMesh*	pSubMesh = new KSubMesh();

		SEARCHTOKEN( "*NODE_TM" );
		FINDTOKEN(); // {
		FINDTOKEN(); // *NODE_NAME
		FINDTOKEN();
		pSubMesh->m_pName = strdup( TOKEN() );

		// Matrix
		pSubMesh->m_Matrix.LoadIdentity();
		SEARCHTOKEN( "*TM_ROW0" );
		FINDTOKEN();	pSubMesh->m_Matrix._11 = (float)atof( TOKEN() );
		FINDTOKEN();	pSubMesh->m_Matrix._12 = (float)atof( TOKEN() );
		FINDTOKEN();	pSubMesh->m_Matrix._13 = (float)atof( TOKEN() );
		SEARCHTOKEN( "*TM_ROW1" );
		FINDTOKEN();	pSubMesh->m_Matrix._21 = (float)atof( TOKEN() );
		FINDTOKEN();	pSubMesh->m_Matrix._22 = (float)atof( TOKEN() );
		FINDTOKEN();	pSubMesh->m_Matrix._23 = (float)atof( TOKEN() );
		SEARCHTOKEN( "*TM_ROW2" );
		FINDTOKEN();	pSubMesh->m_Matrix._31 = (float)atof( TOKEN() );
		FINDTOKEN();	pSubMesh->m_Matrix._32 = (float)atof( TOKEN() );
		FINDTOKEN();	pSubMesh->m_Matrix._33 = (float)atof( TOKEN() );
		SEARCHTOKEN( "*TM_ROW3" );
		FINDTOKEN();	pSubMesh->m_Matrix._41 = (float)atof( TOKEN() );
		FINDTOKEN();	pSubMesh->m_Matrix._42 = (float)atof( TOKEN() );
		FINDTOKEN();	pSubMesh->m_Matrix._43 = (float)atof( TOKEN() );

		SEARCHTOKEN( "}" );

		FINDTOKEN();

		// *MESH
		if( ISTOKEN( "*MESH" ) )
		{
			u32			nVertex;
			KVertex*	pVertex = NULL;

			// Vertex
			SEARCHTOKEN( "*MESH_NUMVERTEX" );
			FINDTOKEN();
			nVertex = atoi( TOKEN() );
			pVertex	= new KVertex[nVertex];

			// Index
			SEARCHTOKEN( "*MESH_NUMFACES" );
			FINDTOKEN();
			nFaces = atoi( TOKEN() );
			
			// Vertex
			SEARCHTOKEN( "*MESH_VERTEX_LIST" );
			FINDTOKEN(); // {
			for( u32 i = 0; i < nVertex; i ++ )
			{
				FINDTOKEN(); // *MESH_VERTEX
				FINDTOKEN(); // Vertex Number

				FINDTOKEN();	pVertex[i].x	= (float)atof( TOKEN() );
				FINDTOKEN();	pVertex[i].y	= (float)atof( TOKEN() );
				FINDTOKEN();	pVertex[i].z	= (float)atof( TOKEN() );
			}
			FINDTOKEN(); // }

			// Index
			KFaceMesh*	pFaceMesh = new KFaceMesh[nFaces];

			SEARCHTOKEN( "*MESH_FACE_LIST" );
			FINDTOKEN(); // {
			for( u32 i = 0; i < nFaces; i ++ )
			{
				pFaceMesh[i].m_pIndex	= new KINDEX[3];

				SEARCHTOKEN( "*MESH_FACE" );
				FINDTOKEN();	// Index Number
				FINDTOKEN();	// A:
				FINDTOKEN();	pFaceMesh[i].m_pIndex[0]	= atoi( TOKEN() );
				FINDTOKEN();	// B:
				FINDTOKEN();	pFaceMesh[i].m_pIndex[1]	= atoi( TOKEN() );
				FINDTOKEN();	// C:
				FINDTOKEN();	pFaceMesh[i].m_pIndex[2]	= atoi( TOKEN() );
				SEARCHTOKEN( "*MESH_MTLID" );
				FINDTOKEN();	pFaceMesh[i].m_MaterialId	= atoi( TOKEN() );
/*				
				if( m_nMaterials )
					pFaceMesh[i].m_MaterialId %= m_nMaterials;
				else
					pFaceMesh[i].m_MaterialId = -1;*/
			}

			// Texture coordinate
			SEARCHTOKEN( "*MESH_NUMTVERTEX" );
			FINDTOKEN();
			u32		nTexCoord;
			
			nTexCoord = atoi( TOKEN() );

			pSubMesh->m_nVertex	= nTexCoord;
			pSubMesh->m_pVertex	= new KVertex[nTexCoord];

			for( u32 i = 0; i < nTexCoord; i ++ )
			{
				SEARCHTOKEN( "*MESH_TVERT" );
				FINDTOKEN();
				FINDTOKEN();	pSubMesh->m_pVertex[i].tu = (float)atof( TOKEN() );
				FINDTOKEN();	pSubMesh->m_pVertex[i].tv = -(float)atof( TOKEN() );
			}

			SEARCHTOKEN( "*MESH_NUMTVFACES" );
			FINDTOKEN();
			u32		nTexFace;
			u32		TexId;

			nTexFace = atoi( TOKEN() );
			if( nTexFace != nFaces )
			{
				KERROR( "LoadASE [%s]: *MESH_NUMTVFACES (%i) != *MESH_NUMFACES (%i)", nTexFace, nFaces );
				return false;
			}

			for( u32 i = 0; i < nFaces; i ++ )
			{
				SEARCHTOKEN( "*MESH_TFACE" );
				FINDTOKEN();

				FINDTOKEN();
				TexId = atoi( TOKEN() );
				pSubMesh->m_pVertex[TexId].x	= pVertex[pFaceMesh[i].m_pIndex[0]].x;
				pSubMesh->m_pVertex[TexId].y	= pVertex[pFaceMesh[i].m_pIndex[0]].y;
				pSubMesh->m_pVertex[TexId].z	= pVertex[pFaceMesh[i].m_pIndex[0]].z;
				pFaceMesh[i].m_pIndex[0]		= TexId;

				FINDTOKEN();
				TexId = atoi( TOKEN() );
				pSubMesh->m_pVertex[TexId].x	= pVertex[pFaceMesh[i].m_pIndex[1]].x;
				pSubMesh->m_pVertex[TexId].y	= pVertex[pFaceMesh[i].m_pIndex[1]].y;
				pSubMesh->m_pVertex[TexId].z	= pVertex[pFaceMesh[i].m_pIndex[1]].z;
				pFaceMesh[i].m_pIndex[1]		= TexId;

				FINDTOKEN();
				TexId = atoi( TOKEN() );
				pSubMesh->m_pVertex[TexId].x	= pVertex[pFaceMesh[i].m_pIndex[2]].x;
				pSubMesh->m_pVertex[TexId].y	= pVertex[pFaceMesh[i].m_pIndex[2]].y;
				pSubMesh->m_pVertex[TexId].z	= pVertex[pFaceMesh[i].m_pIndex[2]].z;
				pFaceMesh[i].m_pIndex[2]		= TexId;
			}

			// Vertex Normal
			SEARCHTOKEN( "*MESH_NORMALS" );
			for( u32 i = 0; i < nFaces; i ++ )
			{
				SEARCHTOKEN( "*MESH_VERTEXNORMAL" );
				FINDTOKEN();
				FINDTOKEN();	pSubMesh->m_pVertex[pFaceMesh[i].m_pIndex[0]].nx	= (float)atof( TOKEN() );
				FINDTOKEN();	pSubMesh->m_pVertex[pFaceMesh[i].m_pIndex[0]].ny	= (float)atof( TOKEN() );
				FINDTOKEN();	pSubMesh->m_pVertex[pFaceMesh[i].m_pIndex[0]].nz	= (float)atof( TOKEN() );
				SEARCHTOKEN( "*MESH_VERTEXNORMAL" );
				FINDTOKEN();
				FINDTOKEN();	pSubMesh->m_pVertex[pFaceMesh[i].m_pIndex[1]].nx	= (float)atof( TOKEN() );
				FINDTOKEN();	pSubMesh->m_pVertex[pFaceMesh[i].m_pIndex[1]].ny	= (float)atof( TOKEN() );
				FINDTOKEN();	pSubMesh->m_pVertex[pFaceMesh[i].m_pIndex[1]].nz	= (float)atof( TOKEN() );
				SEARCHTOKEN( "*MESH_VERTEXNORMAL" );
				FINDTOKEN();
				FINDTOKEN();	pSubMesh->m_pVertex[pFaceMesh[i].m_pIndex[2]].nx	= (float)atof( TOKEN() );
				FINDTOKEN();	pSubMesh->m_pVertex[pFaceMesh[i].m_pIndex[2]].ny	= (float)atof( TOKEN() );
				FINDTOKEN();	pSubMesh->m_pVertex[pFaceMesh[i].m_pIndex[2]].nz	= (float)atof( TOKEN() );
			}

			// Sort the face by Material Id
			qsort( pFaceMesh, nFaces, sizeof( KFaceMesh ), FaceCompare );

			// Count the number of Material Id
			u32	MatId = (u32)-1;
			u32	MatCount = 0;
			for( u32 i = 0; i < nFaces; i ++ )
			{
				if( pFaceMesh[i].m_MaterialId != MatId )
				{
					MatId = pFaceMesh[i].m_MaterialId;
					MatCount ++;
				}
			}

			// Count the number of Faces by Material Id
			pSubMesh->m_nFaceMeshes = MatCount;
			pSubMesh->m_pFaceMeshes = new KFaceMesh[MatCount];
			for( u32 i = 0; i < MatCount; i ++ )
			{
				pSubMesh->m_pFaceMeshes[i].m_nIndex = 0;
			}

			u32 j = (u32)-1;
			MatId = (u32)-1;
			for( u32 i = 0; i < nFaces; i ++ )
			{
				if( pFaceMesh[i].m_MaterialId != MatId )
				{
					MatId = pFaceMesh[i].m_MaterialId;
					j ++;
				}
				pSubMesh->m_pFaceMeshes[j].m_nIndex += 3;
			}

			// Alloc Face by Material Id
			u32	n = 0;
			for( u32 i = 0; i < MatCount; i ++ )
			{
				u32	j = pSubMesh->m_pFaceMeshes[i].m_nIndex;
				pSubMesh->m_pFaceMeshes[i].m_pIndex = new KINDEX[j];

				for( u32 k = 0; k < j; k += 3 )
				{
					pSubMesh->m_pFaceMeshes[i].m_pIndex[k + 0]	= pFaceMesh[n].m_pIndex[0];
					pSubMesh->m_pFaceMeshes[i].m_pIndex[k + 1]	= pFaceMesh[n].m_pIndex[1];
					pSubMesh->m_pFaceMeshes[i].m_pIndex[k + 2]	= pFaceMesh[n].m_pIndex[2];
					pSubMesh->m_pFaceMeshes[i].m_MaterialId		= pFaceMesh[n].m_MaterialId;
					n ++;
				}
			}

			// Free temporary Face
			for( u32 i = 0; i < nFaces; i ++ )
				SafeDeleteArray( pFaceMesh[i].m_pIndex );
			SafeDeleteArray( pFaceMesh );
			SafeDeleteArray( pVertex );
		}

		m_SubMeshes.push_back( pSubMesh );
	}

	m_Parser.Clean();

	return true;
}

//--------------------------------------------------------------------------------------------
int KMesh::FaceCompare( const void* pFace1, const void* pFace2 )
{
	return ((KFaceMesh*)pFace2)->m_MaterialId - ((KFaceMesh*)pFace1)->m_MaterialId;
}

//--------------------------------------------------------------------------------------------
bool KMesh::LoadMSH( char* pFileName, bool bLoadShader )
{
	FILE*	hFile;
	
	hFile = fopen( pFileName, "rb" );
	if( !hFile )
		return false;
	
	// File Header
	char	pHDR[3];
	u32		Version;

	fread( &pHDR, 1, 3, hFile );

	if( memcmp( pHDR, "MSH", 3 ) )
	{
		fclose( hFile );
		KERROR( "LoadMSH: %s\nBad file format\nThis file does not seem to be a MSH file.", pFileName );
		return false;
	}

	fread( &Version, 1, 4, hFile );
	if( Version != KMSHVERSION )
	{
		fclose( hFile );
		KERROR( "LoadMSH: %s\nBad file version\nFile version %i\nExpected version : %i", pFileName, Version, KMSHVERSION );
		return false;
	}

	//
	//	Materials
	//
	fread( &m_nMaterials, 1, sizeof( m_nMaterials ), hFile );
	if( m_nMaterials )
	{
		m_pMaterials = new KMaterialMesh[m_nMaterials];
		for( u32 i = 0; i < m_nMaterials; i ++ )
		{
			// FileName
			char	pBuffer[1024];
			u32		n;

			fread( &n, 1, sizeof( n ), hFile );
			if( n )
			{
				fread( pBuffer, 1, n, hFile );
				pBuffer[n] = 0;
				m_pMaterials[i].m_pFileName = strdup( pBuffer );

				if( bLoadShader )
				{
					m_pRender->LoadShader( m_pMaterials[i].m_pFileName, &m_pMaterials[i].m_pShader );
					m_pMaterials[i].m_bShaderAllocated = true;
				}
			}
		}
	}
	
	//
	//	SubMesh
	//
	u32	nSubMeshes;
	fread( &nSubMeshes, 1, sizeof( nSubMeshes ), hFile );
	for( u32 i = 0; i < nSubMeshes; i ++ )
	{
		KSubMesh*	pSubMesh = new KSubMesh();
		char		pBuffer[1024];
		u32			n;

		m_SubMeshes.push_back( pSubMesh );

		// Name
		fread( &n, 1, sizeof( n ), hFile );
		if( n )
		{
			fread( pBuffer, 1, n, hFile );
			pBuffer[n] = 0;
			pSubMesh->m_pName = strdup( pBuffer );
		}

		// Matrix
		fread( &pSubMesh->m_Matrix, 1, sizeof( KMatrix ), hFile );
		// Vertex
		fread( &pSubMesh->m_nVertex, 1, sizeof( pSubMesh->m_nVertex ), hFile );
		if( pSubMesh->m_nVertex )
		{
			pSubMesh->m_pVertex = new KVertex[pSubMesh->m_nVertex];
			for( u32 j = 0; j < pSubMesh->m_nVertex; j ++ )
				fread( &pSubMesh->m_pVertex[j], 1, sizeof( KVertex ), hFile );
		}
		// FaceMesh
		fread( &pSubMesh->m_nFaceMeshes, 1, sizeof( pSubMesh->m_nFaceMeshes ), hFile );
		if( pSubMesh->m_nFaceMeshes )
		{
			pSubMesh->m_pFaceMeshes = new KFaceMesh[pSubMesh->m_nFaceMeshes];
			for( u32 j = 0; j < pSubMesh->m_nFaceMeshes; j ++ )
			{
				// Material
				fread( &pSubMesh->m_pFaceMeshes[j].m_MaterialId, 1, sizeof( u32 ), hFile );
				// Index
				fread( &pSubMesh->m_pFaceMeshes[j].m_nIndex, 1, sizeof( u32 ), hFile );
				if( pSubMesh->m_pFaceMeshes[j].m_nIndex )
				{
					pSubMesh->m_pFaceMeshes[j].m_pIndex = new KINDEX[pSubMesh->m_pFaceMeshes[j].m_nIndex];
					for( u32 k = 0; k < pSubMesh->m_pFaceMeshes[j].m_nIndex; k ++ )
						fread( &pSubMesh->m_pFaceMeshes[j].m_pIndex[k], 1, sizeof( KINDEX ), hFile );
				}
			}
		}
	}

	fclose( hFile );

	return true;
}

//--------------------------------------------------------------------------------------------
bool KMesh::SaveMSH( char* pFileName )
{
	FILE*	hFile;
	
	hFile = fopen( pFileName, "wb" );
	if( !hFile )
		return false;

	// File Header
	char	pHDR[] = { "MSH" };
	u32		Version = KMSHVERSION;

	fwrite( &pHDR, 1, 3, hFile );
	fwrite( &Version, 1, 4, hFile );

	//
	//	Materials
	//
	fwrite( &m_nMaterials, 1, sizeof( m_nMaterials ), hFile );
	for( u32 i = 0; i < m_nMaterials; i ++ )
	{
		// FileName
		u32	n = (u32)strlen( m_pMaterials[i].m_pFileName );
		fwrite( &n, 1, sizeof( n ), hFile );
		fwrite( m_pMaterials[i].m_pFileName, 1, strlen( m_pMaterials[i].m_pFileName ), hFile );
	}

	//
	//	SubMesh
	//
	u32	nSubMeshes = m_SubMeshes.size();
	fwrite( &nSubMeshes, 1, sizeof( nSubMeshes ), hFile );
	for( u32 i = 0; i < nSubMeshes; i ++ )
	{
		// Name
		u32	n = (u32)strlen( m_SubMeshes[i]->m_pName );
		fwrite( &n, 1, sizeof( n ), hFile );
		fwrite( m_SubMeshes[i]->m_pName, 1, strlen( m_SubMeshes[i]->m_pName ), hFile );
		// Matrix
		fwrite( &m_SubMeshes[i]->m_Matrix, 1, sizeof( KMatrix ), hFile );
		// Vertex
		fwrite( &m_SubMeshes[i]->m_nVertex, 1, sizeof( m_SubMeshes[i]->m_nVertex ), hFile );
		for( u32 j = 0; j < m_SubMeshes[i]->m_nVertex; j ++ )
			fwrite( &m_SubMeshes[i]->m_pVertex[j], 1, sizeof( KVertex ), hFile );
		// FaceMesh
		fwrite( &m_SubMeshes[i]->m_nFaceMeshes, 1, sizeof( m_SubMeshes[i]->m_nFaceMeshes ), hFile );
		for( u32 j = 0; j < m_SubMeshes[i]->m_nFaceMeshes; j ++ )
		{
			// Material
			fwrite( &m_SubMeshes[i]->m_pFaceMeshes[j].m_MaterialId, 1, sizeof( u32 ), hFile );
			// Index
			fwrite( &m_SubMeshes[i]->m_pFaceMeshes[j].m_nIndex, 1, sizeof( u32 ), hFile );
			for( u32 k = 0; k < m_SubMeshes[i]->m_pFaceMeshes[j].m_nIndex; k ++ )
				fwrite( &m_SubMeshes[i]->m_pFaceMeshes[j].m_pIndex[k], 1, sizeof( KINDEX ), hFile );
		}
	}

	fclose( hFile );

	return true;
}
