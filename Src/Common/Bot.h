//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Bot.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 16/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__BOT_H__
#define	__BOT_H__

#include "Item.h"
#include "BotDefs.h"

class KLight;

extern KCOLOR g_pBotColor[];

//--------------------------------------------------------------------------------------------
class KBot : public KItem
{
protected:
	s32					m_Vitality;
	s32					m_Life;
	s32					m_Power;
	s32					m_Speed;
	s32					m_Vision;
	char				m_pName[BOT_NAME_LENGTH];
	KBOTACTION			m_Action;
	KBOTATTACKTARGET	m_AttackTarget;
	u32					m_Number;
	bool				m_bUseBinocular;
	s32					m_FireRange;
	bool				m_bCheater;
	KLight*				m_pLight;

	static KMesh*		m_pMesh;
	static KMesh*		m_pGraveMesh;
	static KShader*		m_pShader[9];

public:
	KBot( KRender* pRender, KLevel* pLevel );
	virtual ~KBot();

	virtual void		Display();
	virtual void		Display( KPt& Pos );
	virtual void		Display2D();
	virtual void		Display2D( KPt& Pos, KPt& Size );
	static bool			Load( KRender* pRender );
	static void			Unload();
						
	void				SetpName( char* pName )							{ strcpy( m_pName, pName );		}
	char*				GetpName()										{ return m_pName;				}
																		
	void				SetAction( KBOTACTION Action )					{ m_Action = Action;			}
	KBOTACTION			GetAction()										{ return m_Action;				}
						
	void				SetAttackTarget( KBOTATTACKTARGET Target )		{ m_AttackTarget = Target;		}
	KBOTATTACKTARGET	GetAttackTarget()								{ return m_AttackTarget;		}

	void				SetVitality( s32 Vitality )						{ m_Vitality = Vitality;		}
	s32					GetLife()										{ return m_Life;				}
	
	void				SetVision( s32 Vision )							{ m_Vision = Vision;			}
	s32					GetVision();
	s32					GetSight();

	void				SetPower( s32 Power )							{ m_Power = Power;				}
	s32					GetPower();

	void				SetSpeed( s32 Speed )							{ m_Speed = Speed;				}
	s32					GetSpeed();

	void				SetLife( s32 Life )								{ m_Life = Life;				}

	void				SetNumber( u32 Number )							{ m_Number = Number;			}

	void				Use( KItem* pItem );

	bool				IsUseBinocular()								{ return m_bUseBinocular;		}
	void				SetbUseBinocular( bool bUse )					{ m_bUseBinocular = bUse;		}

	s32					GetFireRange()									{ return m_FireRange;			}
	void				SetFireRange( s32 Range )						{ m_FireRange = Range;			}

	void				SetbCheater( bool bCheater )					{ m_bCheater = bCheater;		}
	bool				IsCheater()										{ return m_bCheater;			}

	s32					GetMaxLife();
};


#endif // __BOT_H__
