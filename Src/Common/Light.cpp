//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Light.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 11/08/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "Render.h"
#include "Light.h"

#define	MAX_LIGHTS	8
//--------------------------------------------------------------------------------------------
//u32 g_LightNextIndex = GL_LIGHT0;

KLight*	g_pLight[MAX_LIGHTS] = { NULL };

//--------------------------------------------------------------------------------------------
KLight::KLight( KRender* pRender )
{
	m_pRender		= pRender;
	m_Id			= -1;//g_LightNextIndex ++;

	for( s32 i = 0; i < MAX_LIGHTS; i ++ )
	{
		if( !g_pLight[i] )
		{
			g_pLight[i] = this;
			m_Id = GL_LIGHT0 + i;
			break;
		}
	}

	m_Type			= KLT_POINT;
	m_Ambient.m_R	= m_Ambient.m_G = m_Ambient.m_B = m_Ambient.m_A = 1.0f;
	m_Diffuse.m_R	= m_Diffuse.m_G = m_Diffuse.m_B = m_Diffuse.m_A = 0.0f;
	m_Specular.m_R	= m_Specular.m_G = m_Specular.m_B = m_Specular.m_A = 1.0f;

	m_Position		= KVector( 0.0f, 0.0f, 0.0f );
	m_Direction		= KVector( 0.0f, 1.0f, 0.0f );

	if( m_Id == -1 )
	{
		KERROR( "Cannot create more lights\n" );
		return;
	}
/*
	glLightf( m_Id, GL_SPOT_CUTOFF, 180.0f );
	glLightf( m_Id, GL_SPOT_EXPONENT, 10.0f );
*/
//	glLightf( m_Id, GL_CONSTANT_ATTENUATION, 10.0f );
	glLightf( m_Id, GL_LINEAR_ATTENUATION, 0.05f );
//	glLightf( m_Id, GL_QUADRATIC_ATTENUATION, 0.01f );

	SetPosition( m_Position );
	SetDirection( m_Direction );
	
	SetAmbient( KRGB( 128, 128, 128 ) );
	SetDiffuse( KRGB( 255, 255, 255 ) );

	Enable( true );
}

//--------------------------------------------------------------------------------------------
KLight::~KLight()
{
	if( m_Id >= 0 )
	{
		g_pLight[m_Id - GL_LIGHT0] = NULL;
	}

	Enable( false );
}

//--------------------------------------------------------------------------------------------
void KLight::Enable( bool bEnable )
{
	if( m_Id == -1 )
		return;

	if( bEnable )
		glEnable( m_Id );
	else
		glDisable( m_Id );
}

//--------------------------------------------------------------------------------------------
void KLight::SetType( KLIGHTTYPE Type )
{
	if( m_Id == -1 )
		return;

	m_Type = Type;
//	m_pRender->GetpDevice()->SetLight( m_Id, &m_Light );
}

//--------------------------------------------------------------------------------------------
void KLight::SetPosition( KVector& Position )
{
	if( m_Id == -1 )
		return;

	GLfloat	fPos[4];

	m_Position = Position;

	fPos[0] = m_Position.x;
	fPos[1] = m_Position.y;
	fPos[2] = m_Position.z;
	fPos[3] = 1.0f;

	glLightfv( m_Id, GL_POSITION, fPos );
}

//--------------------------------------------------------------------------------------------
void KLight::SetDirection( KVector& Direction )
{
	if( m_Id == -1 )
		return;

	m_Direction = Direction;
	glLightfv( m_Id, GL_SPOT_DIRECTION, (GLfloat*)&m_Direction );
}

//--------------------------------------------------------------------------------------------
void KLight::SetAmbient( KCOLOR Ambient )
{
	if( m_Id == -1 )
		return;

	m_Ambient.m_R = (float)KGETR( Ambient ) / 255.0f;
	m_Ambient.m_G = (float)KGETG( Ambient ) / 255.0f;
	m_Ambient.m_B = (float)KGETB( Ambient ) / 255.0f;
	m_Ambient.m_A = (float)KGETA( Ambient ) / 255.0f;
	glLightfv( m_Id, GL_AMBIENT, (GLfloat*)&m_Ambient );
}

//--------------------------------------------------------------------------------------------
void KLight::SetDiffuse( KCOLOR Diffuse )
{
	if( m_Id == -1 )
		return;

	m_Diffuse.m_R = (float)KGETR( Diffuse ) / 255.0f;
	m_Diffuse.m_G = (float)KGETG( Diffuse ) / 255.0f;
	m_Diffuse.m_B = (float)KGETB( Diffuse ) / 255.0f;
	m_Diffuse.m_A = (float)KGETA( Diffuse ) / 255.0f;
	glLightfv( m_Id, GL_DIFFUSE, (GLfloat*)&m_Diffuse );
}
