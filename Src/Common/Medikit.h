//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Medikit.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 20/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__MEDIKIT_H__
#define	__MEDIKIT_H__

#include "Item.h"
#include "BotDefs.h"

//--------------------------------------------------------------------------------------------
class KMedikit : public KItem
{
protected:
	s32						m_HealLife;
	static KMesh*			m_pMesh;

public:
	KMedikit( KRender* pRender, KLevel* pLevel );
	virtual ~KMedikit();

	virtual void			Display( KPt& Pos );
	virtual void			Display2D( KPt& Pos, KPt& Size );
	static bool				Load( KRender* pRender );
	static void				Unload();

	s32						GetHealLife()					{ return m_HealLife;		}
};

#endif // __MEDIKIT_H__
