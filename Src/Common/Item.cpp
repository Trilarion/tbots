//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Item.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 16/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "Level.h"
#include "Item.h"

//--------------------------------------------------------------------------------------------
KItem::KItem( KRender* pRender, KLevel* pLevel )
{
	m_pFather	= NULL;
	m_pRender	= pRender;
	m_pLevel	= pLevel;
	m_Pos		= KPt( 0, 0 );
	m_PrevPos	= m_Pos;
	m_StartPos	= 0;
	m_EndPos	= 0;
	m_Type		= KIT_NONE;
	m_bTakeAway	= false;
	m_bVisible	= true;

	m_Matrix.LoadIdentity();
}

//--------------------------------------------------------------------------------------------
KItem::~KItem()
{
	KItem* pItem;

	while( ( pItem = m_Childs.GetFirst() ) )
	{
		m_Childs.Remove( pItem );
		SafeDelete( pItem );
	}
}

//--------------------------------------------------------------------------------------------
void KItem::SetPos( KPt& Pos, KTIME Delay )
{
	m_PrevPos	= m_Pos;
	m_Pos		= Pos;
	m_StartPos	= g_Timer.GetTime();
	m_EndPos	= m_StartPos + Delay;
}

//--------------------------------------------------------------------------------------------
KFPt KItem::GetInterpoledPos()
{
	KTIME	Time = g_Timer.GetTime();
	
	if( Time < m_StartPos )
		return KFPt( (float)m_PrevPos.x, (float)m_PrevPos.y );

	if( Time > m_EndPos )
		return KFPt( (float)m_Pos.x, (float)m_Pos.y );

	float	Delta = (float)(Time - m_StartPos) / (float)(m_EndPos - m_StartPos);

	return KFPt(	(float)(m_Pos.x - m_PrevPos.x) * Delta + (float)m_PrevPos.x,
					(float)(m_Pos.y - m_PrevPos.y) * Delta + (float)m_PrevPos.y );
}

//--------------------------------------------------------------------------------------------
void KItem::Display()
{
	KPt	Pt( m_Pos.x * CELL_SIZE, m_Pos.y * CELL_SIZE );

	Display( Pt );
}

//--------------------------------------------------------------------------------------------
void KItem::Display2D()
{
	KPt	Pos( SCREEN_OFFSETX + m_Pos.x * CELL_SIZE, m_Pos.y * CELL_SIZE );
	KPt	Size( CELL_SIZE, CELL_SIZE );
	
	Display2D( Pos, Size );
}

//--------------------------------------------------------------------------------------------
void KItem::SetMatrix( KPt& Pos, float PosZ, float AngleX, float AngleY, float AngleZ )
{
	s32 y = m_pLevel->GetHeight() * CELL_SIZE - Pos.y + CELL_SIZE / 2;

	m_Matrix.LoadIdentity();
	m_Matrix.RotX( AngleX );
	m_Matrix.RotY( AngleZ );
	m_Matrix.RotZ( AngleY );
	m_Matrix.Trans( Pos.x + CELL_SIZE / 2.0f, (float)y, PosZ );
}
