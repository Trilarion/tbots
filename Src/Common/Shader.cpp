//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Shader.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 05/08/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "PngFile.h"
#include "Render.h"
#include "Shader.h"

//--------------------------------------------------------------------------------------------
KShader::KShader()
{
	m_TexId = 0;
	glGenTextures( 1, &m_TexId );
}

//--------------------------------------------------------------------------------------------
KShader::~KShader()
{
	Free();
	glDeleteTextures( 1, &m_TexId );
}

//--------------------------------------------------------------------------------------------
KRESULT KShader::LoadShader( KRender* pRender, char* pFileName )
{
	KASSERT( pRender );

	KPngFile	Png;
	
	Png.Load( pFileName );

	s32	Bpp = Png.GetDepth() * Png.GetChannels();

	if( Bpp < 24 )
	{
		KERROR( "KShader::LoadShader(...)\nCannot load png file : %s", pFileName );
		return KR_TEXTUREFAILED;
	}

	m_Info.m_Width	= Png.GetWidth();
	m_Info.m_Height	= Png.GetHeight();

	glBindTexture( GL_TEXTURE_2D, m_TexId );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST );

	if( Bpp == 24 )
		gluBuild2DMipmaps( GL_TEXTURE_2D, 3, m_Info.m_Width, m_Info.m_Height, GL_RGB, GL_UNSIGNED_BYTE, Png.GetpPixels() );
	
	if( Bpp == 32 )
		gluBuild2DMipmaps( GL_TEXTURE_2D, 4, m_Info.m_Width, m_Info.m_Height, GL_RGBA, GL_UNSIGNED_BYTE, Png.GetpPixels() );

	return KR_OK;
}

//--------------------------------------------------------------------------------------------
void KShader::Free()
{
}
