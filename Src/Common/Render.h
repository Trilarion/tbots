//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: RenderOGL.h
//	Author			: - Dino			dino@zythum-project.com
//
//	Date			: 26/10/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__RENDER_H__
#define	__RENDER_H__

#include "RenderDefs.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>

#ifdef _LINUX
#include <GL/glx.h>
#include <X11/Xlib.h>

typedef Window		HWND;
typedef GLXContext	HGLRC;
typedef Display*	HDC;
#endif // _LINUX

//--------------------------------------------------------------------------------------------
class KRender
{
protected:
#ifdef _WIN32
	PIXELFORMATDESCRIPTOR		m_PFD;
#endif // _WIN32
	HWND						m_hWnd;
	HGLRC						m_hGLRC;
	HDC							m_hDC;
								
	u32							m_Width;
	u32							m_Height;
	u32							m_Bpp;
	bool						m_bFullScreen;
								
	u32							m_nPoly;
	u32							m_nPrimitive;
								
	u32							m_MaxVertex;
	u32							m_MaxIndex;
								
	// Vertex					
	u32							m_nVertex;
	u32							m_nIndex;
	KVertex*					m_pVertexBuffer;
	KINDEX*						m_pIndexBuffer;

	// TLVertex
	u32							m_nTLVertex;
	u32							m_nTLIndex;
	KTLVertex*					m_pTLVertexBuffer;
	KINDEX*						m_pTLIndexBuffer;

	KRESULT						DrawPrimitive();
	KRESULT						DrawPrimitiveTL();

	KShader*					m_pFontShader;

	KCamera*					m_pCamera;

public:
	KRender();

	KRESULT						Init( HWND hWnd, HDC hDC, u32 Width, u32 Height, u32 Bpp, bool bFullScreen );
	KRESULT						Reset( HWND hWnd, u32 Width, u32 Height, u32 Bpp, bool bFullScreen );
	KRESULT						End();
	KRESULT						BeginScene();
	KRESULT						EndScene();
	KRESULT						InitScene();
								
	KRESULT						Flip( HWND hWnd = 0, KRect* pRect = NULL );
	KRESULT						SetProjectionMatrix( float fFOV, float fAspect, float fNearPlane, float fFarPlane );
	KRESULT						DrawTriangles( KRenderFace& Face );
	KRESULT						FlushTriangles();
	KRESULT						DrawTrianglesTL( KRenderFace& Face );
	KRESULT						FlushTrianglesTL();
								
	KRESULT						SetShader( KShader* pShader );
	KRESULT						LoadShader( char* pFileName, KShader** ppShader );
//	KRESULT						FreeShader( KShader* pShader );
								
	KRESULT						LoadMesh( char* pFileName, KMesh** ppMesh, bool bLoadShader = true );
//	KRESULT						FreeMesh( KMesh* pMesh );
	KRESULT						DrawMesh( KMatrix& Matrix, KMesh* pMesh );
	KRESULT						DrawSubMesh( KMatrix& Matrix, KMesh* pMesh, u32 MeshId );
								
	KRESULT						DrawQuad( s32 x, s32 y, s32 sx, s32 sy, KShader* pShader = NULL, KCOLOR Color = KRGBA( 255, 255, 255, 255 ), KCOLOR Specular = KRGBA( 0, 0, 0, 0 ), KRMODE RenderMode = KRM_NORMAL, float *pTU = NULL, float *pTV = NULL );
								
	KRESULT						DrawText( s32 x, s32 y, char* pText, s32 Size = 16 );
								
	void						SetpCamera( KCamera* pCamera )	{ m_pCamera = pCamera;	}
																
	KRESULT						ActivateLighting( bool bActivate );
	KRESULT						ActivateWireFrame( bool bActivate );

	u32							GetnPolys()		{ return m_nPoly;	}
};

#endif // __RENDER_H__
