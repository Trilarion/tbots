//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: PngFile.h
//	Author			: - Dino			dino@zythum-project.com
//
//	Date			: 26/10/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__PNGFILE_H__
#define	__PNGFILE_H__

#include "Types.h"
#include <png.h>

//--------------------------------------------------------------------------------------------
class KPngFile
{
protected:
	FILE*		m_hFile;
	s32			m_Width;
	s32			m_Height;
	s32			m_Depth;
	s32			m_Channels;
	u8*			m_pPixels;

	static void ReadDataFunc( png_structp png_ptr, png_bytep data, png_size_t length );

public:
	KPngFile();
	~KPngFile();

	bool		Load( char* pFileName );

	s32			GetWidth()		{ return m_Width;		}
	s32			GetHeight()		{ return m_Height;		}
	s32			GetDepth()		{ return m_Depth;		}
	s32			GetChannels()	{ return m_Channels;	}
	u8*			GetpPixels()	{ return m_pPixels;		}
};


#endif // __PNGFILE_H__
