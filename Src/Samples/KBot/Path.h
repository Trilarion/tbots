//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Path.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 17/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifndef	__PATH_H__
#define	__PATH_H__

#include "Types.h"
#include "Level.h"
#include <vector>

using namespace std;

class KItem;

//--------------------------------------------------------------------------------------------
typedef enum _KPATHFINDERRESULT
{
	KPFR_OK,
	KPFR_CANNOT
} KPATHFINDERRESULT;

//--------------------------------------------------------------------------------------------
class KPath
{
protected:
	vector<KPt>		m_Path;
	u32				m_Size;

public:
	KPath()
	{
		m_Size = 0;
	};

	~KPath()
	{
		m_Path.clear();
	};

	void	Dump()
	{
		for( u32 i = 0; i < m_Size; i ++ )
		{
			char	s[256];
			sprintf( s, "%i	: %.2i %.2i\n", i, m_Path[i].x, m_Path[i].y );
#ifdef _WIN32
			OutputDebugString( s );
#endif // _WIN32
#ifdef _LINUX
			printf( "%s\n", s );
#endif // _LINUX
		}
	}

	void			AddPath( KPt Pos )		{ m_Path.push_back( Pos ); m_Size ++;	}
	void			RemoveLastPos()			{ m_Path.pop_back(); m_Size --;			}
	u32				GetSize()				{ return (u32)m_Path.size();			}
	KPt				GetPos( u32 i )			{ return m_Path[i];						}
};

//--------------------------------------------------------------------------------------------
class KPathFinder
{
protected:
	CellType*			m_pCell;
	KCell*				m_pMapCell;
	u32					m_Width;
	u32					m_Height;

	KCell*				GetpMapCell( u32 Width, u32 Height )				{ return &m_pMapCell[Height * m_Width + Width];	}
	CellType			GetCell( u32 Width, u32 Height )					{ return m_pCell[Height * m_Width + Width];		}
	void				SetCell( u32 Width, u32 Height, CellType Value )	{ m_pCell[Height * m_Width + Width] = Value;	}
	KPATHFINDERRESULT	ScanPath( KPath* pPath, u32 x, u32 y, u32 dstx, u32 dsty );

public:
	KPathFinder( KCell* pMapCell, s32 Width, s32 Height );
	~KPathFinder();

	KPath*				FindPath( u32 srcx, u32 srcy, u32 dstx, u32 dsty );

};

#endif // __PATH_H__
