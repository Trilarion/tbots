//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Path.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 17/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#include "Path.h"
#include "Level.h"

//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
KPathFinder::KPathFinder( KCell* pMapCell, s32 Width, s32 Height )
{
	m_pMapCell	= pMapCell;
	m_Width		= Width;
	m_Height	= Height;
	m_pCell		= new CellType[m_Width * m_Height];
}

//--------------------------------------------------------------------------------------------
KPathFinder::~KPathFinder()
{
	Deletep( m_pCell );
}

//--------------------------------------------------------------------------------------------
KPath* KPathFinder::FindPath( u32 srcx, u32 srcy, u32 dstx, u32 dsty )
{
	memset( m_pCell, 0, m_Width * m_Height * sizeof( CellType ) );

	for(u32 y = 0; y < m_Height; y ++ )
	for(u32 x = 0; x < m_Width; x ++ )
	{
		SetCell( x, y, GetpMapCell( x, y )->GetType() != 0 );
	}
	
	KPath*	pPath = new KPath();

	if( ScanPath( pPath, srcx, srcy, dstx, dsty ) == KPFR_OK )
		return pPath;

	Deletep( pPath );

	return NULL;
}

//--------------------------------------------------------------------------------------------
s8 dirx[4];// = { 1, 0, -1, 0 };
s8 diry[4];// = { 0, 1, 0, -1 };
#define	SET_DIR( _dir, _d1, _d2, _d3, _d4 )	_dir[0] = _d1;_dir[1] = _d2;_dir[2] = _d3;_dir[3] = _d4

//--------------------------------------------------------------------------------------------
KPATHFINDERRESULT KPathFinder::ScanPath( KPath* pPath, u32 x, u32 y, u32 dstx, u32 dsty )
{
	// Chemin trouv�
	if( ( x == dstx ) && ( y == dsty ) )
	{
		pPath->AddPath( KPt( x, y ) );
		return KPFR_OK;
	}

	// Cellule interdite
	if( GetCell( x, y ) != 0 )
		return KPFR_CANNOT;

	// Cellule occup�e
//	KItem*	pItem = m_pLevel->GetpUnit( x, y );
//	if( pUnit && pUnit != pMyUnit )
//		return KPFR_CANNOT;

	// La cellule est libre, on la marque
	SetCell( x, y, 2 );
	pPath->AddPath( KPt( x, y ) );

	// Cherche a gauche, a droite, en haut et en bas
	s32 dx = dstx - x;
	s32 dy = dsty - y;

	// Optimise les directions
	if( ( dx * dx ) > ( dy * dy ) )
	{
		// droite
		if( dx > 0 )
		{
			SET_DIR( dirx, 1, 0, 0, -1 );
			if( dy > 0 )
			{
				SET_DIR( diry, 0, 1, -1, 0 );
			}
			else
			{
				SET_DIR( diry, 0, -1, 1, 0 );
			}
		}
		// gauche
		else
		{
			SET_DIR( dirx, -1, 0, 0, 1 );
			if( dy > 0 )
			{
				SET_DIR( diry, 0, 1, -1, 0 );
			}
			else
			{
				SET_DIR( diry, 0, -1, 1, 0 );
			}
		}
	}
	else
	{
		// bas
		if( dy > 0 )
		{
			SET_DIR( diry, 1, 0, 0, -1 );
			if( dx > 0 )
			{
				SET_DIR( dirx, 0, 1, -1, 0 );
			}
			else
			{
				SET_DIR( dirx, 0, -1, 1, 0 );
			}
		}
		// haut
		else
		{
			SET_DIR( diry, -1, 0, 0, 1 );
			if( dx > 0 )
			{
				SET_DIR( dirx, 0, 1, -1, 0 );
			}
			else
			{
				SET_DIR( dirx, 0, -1, 1, 0 );
			}
		}
	}

	for( u32 dir = 0; dir < 4; dir ++ )
	{
		KPATHFINDERRESULT	r = ScanPath( pPath, x + dirx[dir], y + diry[dir], dstx, dsty );

		if( r == KPFR_OK )
			return KPFR_OK;
	}

	pPath->RemoveLastPos();

	return KPFR_CANNOT;
}
