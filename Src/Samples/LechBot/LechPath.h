//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: LechPath.h
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 21/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------

#ifndef __LECHPATH_H
#define __LECHPATH_H


#include <algorithm>
#include <vector>

using namespace std;


////////////////////////////////////////////////////////////////////////////////
// Defines & Enumerations
////////////////////////////////////////////////////////////////////////////////

enum LechPathState
{
	LPS_NOT_INITIALISED,
	LPS_SEARCHING,
	LPS_SUCCEEDED,
	LPS_FAILED,
	LPS_OUT_OF_MEMORY,
	LPS_INVALID
};


//==============================================================================
//
// 							 Classe LechPathFinder
//
//						    Bas� sur un algorithme A*
//
//==============================================================================
class LechPathFinder
{
public:

	////////////////////////////////////////////////////////////////////////////////
	// 							   Classe SearchNode
	////////////////////////////////////////////////////////////////////////////////
	class SearchNode
	{
	public:
		KPt		m_Position;
		
		SearchNode()										{ m_Position = KPt(0,0); }
		SearchNode(KPt Position)							{ m_Position = Position; }
		SearchNode(s32 x, s32 y)							{ m_Position = KPt(x,y); }

		inline float	GoalDistance(SearchNode &node)		{ return (float)m_Position.SquareDistance(node.m_Position); }
		inline bool		isSameState(SearchNode &node)		{ return (m_Position == node.m_Position); }
		inline bool		isGoal(SearchNode &node)			{ return (m_Position == node.m_Position); }

		
		// R�up�e tous les successeurs d'un node

		void GetSuccessors(LechPathFinder *pPathFinder, SearchNode *pParentNode)
		{
			KPt Parent(-1,-1);

			if (pParentNode)
				Parent = pParentNode->m_Position;

			SearchNode NewNode;

			// On va chercher un peu partout autour ...

			if ( (pPathFinder->m_pMap->GetCost(m_Position.x - 1, m_Position.y) < PATH_MAX_COST) && !((Parent.x == m_Position.x - 1) && (Parent.y == m_Position.y)) ) 
			{
				NewNode = SearchNode(m_Position.x - 1 ,m_Position.y);
				pPathFinder->AddSuccessor(NewNode);
			}	

			if ( (pPathFinder->m_pMap->GetCost(m_Position.x, m_Position.y - 1) < PATH_MAX_COST) && !((Parent.x == m_Position.x) && (Parent.y == m_Position.y - 1)) ) 
			{
				NewNode = SearchNode(m_Position.x, m_Position.y - 1);
				pPathFinder->AddSuccessor(NewNode);
			}	

			if ( (pPathFinder->m_pMap->GetCost(m_Position.x + 1, m_Position.y) < PATH_MAX_COST) && !((Parent.x == m_Position.x + 1) && (Parent.y == m_Position.y)) ) 
			{
				NewNode = SearchNode(m_Position.x + 1, m_Position.y);
				pPathFinder->AddSuccessor(NewNode);
			}	

			if ( (pPathFinder->m_pMap->GetCost(m_Position.x, m_Position.y + 1) < PATH_MAX_COST) && !((Parent.x == m_Position.x) && (Parent.y == m_Position.y + 1)) )
			{
				NewNode = SearchNode(m_Position.x, m_Position.y + 1);
				pPathFinder->AddSuccessor(NewNode);
			}	
		}
	};

	////////////////////////////////////////////////////////////////////////////////
	// 									Classe Node
	////////////////////////////////////////////////////////////////////////////////
	class Node
	{
	public:
			Node *m_pParent;
			Node *m_pChild;
			
			float m_NodeCost;		// Cot associ��un node (et de ses pr�ecesseurs)
			float m_Distance;		// Distance estim� jusqu'au but
			float m_CostCumul;		// Cumul des cots (NodeCost + Distance)

			Node()
			{
				m_pParent	= 0;
				m_pChild	= 0;
				m_NodeCost	= 0.0f;
				m_Distance	= 0.0f;
				m_CostCumul	= 0.0f;	// Que de 0 !!! LechBot est tr� mauvais �l'�ole !
			}

			SearchNode m_SearchNode;
	};


public:
	LechMap			*m_pMap;

	
	LechPathFinder()
	{
		m_State					= LPS_NOT_INITIALISED;
		m_pCurrentSolutionNode	= NULL;
		m_pMap					= NULL;
	}

	u32		GetStepCount()												{ return m_Steps; }
	
	bool	operator() (const Node *pNode1, const Node *pNode2) const	{ return pNode1->m_CostCumul > pNode2->m_CostCumul; }

	void	SetMap(LechMap *pMap)										{ m_pMap = pMap; }

	
	// Initialiser le PathFinder (�at initial)

	void SetStartAndGoalNodes(SearchNode &Start, SearchNode &Goal)
	{
		m_pStart				= new Node;
		m_pStart->m_SearchNode	= Start;

		m_pGoal					= new Node;
		m_pGoal->m_SearchNode	= Goal;

		m_State					= LPS_SEARCHING;

		m_Steps					= 0;
		
		// Initialiser les donn�s du PathFinder
		m_pStart->m_NodeCost	= 0; 
		m_pStart->m_Distance	= m_pStart->m_SearchNode.GoalDistance(m_pGoal->m_SearchNode);
		m_pStart->m_CostCumul	= m_pStart->m_NodeCost + m_pStart->m_Distance;
		m_pStart->m_pParent		= NULL;

		// Placer le node de d�art dans l'OpenList (et trier la liste tant qu'�faire !)
		m_OpenList.push_back(m_pStart);
		push_heap(m_OpenList.begin(), m_OpenList.end(), LechPathFinder());

	}

	// Ajouter un successeur dans la liste des successeurs (wow! on aurait eu du mal �deviner)

	void AddSuccessor(SearchNode &NewNode)
	{
		Node *node = new Node;

		node->m_SearchNode = NewNode;

		m_Successors.push_back(node);
	}

	// Ex�uter une �ape de la recherche

	LechPathState SearchStep()
	{
		// Si le PathFinder n'est pas initialis�-> on sort !
		if ( (m_State == LPS_NOT_INITIALISED) || (m_State >= LPS_INVALID) )
			return m_State;

		// Si la recherche est termin� -> on sort !!
		if ( (m_State == LPS_SUCCEEDED) || (m_State == LPS_FAILED) )
			return m_State; 

		// Si l'OpenList est vide -> on sort j'ai dis !!!
		if ( m_OpenList.empty() )
		{
			FreeAllNodes();
			m_State = LPS_FAILED;
			return m_State;
		}
		
		// C'est pas vrai ca ... alors on continue la recherche puisqu'il veut pas sortir!
		m_Steps++;
		
		// M�oriser le meilleur node dans l'OpenList (celui qui a le plus bas CostCumul)
		// Bah vi, moins c'est cher mieux c'est !
		Node *n = m_OpenList.front();
		pop_heap(m_OpenList.begin(), m_OpenList.end(), LechPathFinder());
		m_OpenList.pop_back();

		// V�ifier si le but n'est pas atteint (on va quand m�e pas continuer �chercher pour rien)
		if ( n->m_SearchNode.isGoal(m_pGoal->m_SearchNode) )
		{
			m_pGoal->m_pParent = n->m_pParent;

			if ( n != m_pStart )
			{
				delete n;

				Node *pNodeChild	= m_pGoal;
				Node *pNodeParent	= m_pGoal->m_pParent;

				do 
				{
					pNodeParent->m_pChild = pNodeChild;

					pNodeChild	= pNodeParent;
					pNodeParent = pNodeParent->m_pParent;
				} 
				while( pNodeChild != m_pStart );
			}

			FreeUnusedNodes();

			m_State = LPS_SUCCEEDED;

			return m_State;
		}
		else
		{
			// Bon apparement on l'a pas trouv�... :'(
			m_Successors.clear();

			n->m_SearchNode.GetSuccessors(this, n->m_pParent?&n->m_pParent->m_SearchNode:NULL);

			for (vector<Node *>::iterator Successor = m_Successors.begin(); Successor != m_Successors.end(); Successor++)
			{
				float NewCost = n->m_NodeCost + m_pMap->GetCost(n->m_SearchNode.m_Position);

				// Rechercher le node dans l'OpenList
				vector <Node *>::iterator OpenListResult;
				for ( OpenListResult = m_OpenList.begin(); OpenListResult != m_OpenList.end(); OpenListResult++)
				{
					if ( (*OpenListResult)->m_SearchNode.isSameState((*Successor)->m_SearchNode) )
						break;					
				}

				if ( OpenListResult != m_OpenList.end() )
				{
					if ( (*OpenListResult)->m_NodeCost <= NewCost )
					{
						delete (*Successor);
						continue;
					}
				}

				// Rechercher le node dans la ClosedList
				vector <Node *>::iterator ClosedListResult;
				for ( ClosedListResult = m_ClosedList.begin(); ClosedListResult != m_ClosedList.end(); ClosedListResult++)
				{
					if ( (*ClosedListResult)->m_SearchNode.isSameState((*Successor)->m_SearchNode) )
						break;					
				}

				if ( ClosedListResult != m_ClosedList.end() )
				{
					if ( (*ClosedListResult)->m_NodeCost <= NewCost )
					{
						delete (*Successor);
						continue;
					}
				}

				// Le node trouv�est le meilleur -> le m�oriser
				(*Successor)->m_pParent		= n;
				(*Successor)->m_NodeCost	= NewCost;
				(*Successor)->m_Distance	= (*Successor)->m_SearchNode.GoalDistance(m_pGoal->m_SearchNode);
				(*Successor)->m_CostCumul	= (*Successor)->m_NodeCost + (*Successor)->m_Distance;

				// Supprimer le successeur de la ClosedList (s'il s'y trouve)
				if ( ClosedListResult != m_ClosedList.end() )
				{
					delete (*ClosedListResult);
					m_ClosedList.erase(ClosedListResult);
				}

				// Mettre �jour les infos de l'ancien node
				if ( OpenListResult != m_OpenList.end() )
				{	   
					delete (*OpenListResult);
			   		
					m_OpenList.erase(OpenListResult);
					make_heap(m_OpenList.begin(), m_OpenList.end(), LechPathFinder());
				}

				m_OpenList.push_back((*Successor));
				push_heap(m_OpenList.begin(), m_OpenList.end(), LechPathFinder());
			}

			m_ClosedList.push_back(n);
		}

 		return m_State;
	}

	// Lib�er tous les nodes solution

	void FreeSolutionNodes()
	{
		Node *n = m_pStart;

		if ( m_pStart->m_pChild )
		{
			Node *del;

			while (n)
			{
				del = n;
				n = n->m_pChild;

				delete del; del = NULL;
			}
		}
		else
		{
			delete m_pStart;
			delete m_pGoal;
		}
	}



	// Fonctions de parcours dans la liste des nodes solution


	// R�up�er le premier node du Path trouv�
	SearchNode *GetSolutionStart()
	{
		m_pCurrentSolutionNode = m_pStart;

		if ( m_pStart )		return &m_pStart->m_SearchNode;
		else				return NULL;
	}
	
	// R�up�er le dernier node du Path trouv�
	SearchNode *GetSolutionEnd()
	{
		m_pCurrentSolutionNode = m_pGoal;
		
		if ( m_pGoal )		return &m_pGoal->m_SearchNode;
		else				return NULL;
	}
	
	// R�up�er le node pr��ent dans le parcours du Path trouv�
	SearchNode *GetSolutionPrev()
	{
		if ( m_pCurrentSolutionNode )
		{
			if ( m_pCurrentSolutionNode->m_pParent )
			{
				Node *pParent = m_pCurrentSolutionNode->m_pParent;

				m_pCurrentSolutionNode = m_pCurrentSolutionNode->m_pParent;

				return &pParent->m_SearchNode;
			}
		}
		return NULL;
	}

	// R�up�er le node pr��ent dans le parcours du Path trouv�
	SearchNode *GetSolutionNext()
	{
		if ( m_pCurrentSolutionNode )
		{
			if ( m_pCurrentSolutionNode->m_pChild )
			{
				Node *pChild = m_pCurrentSolutionNode->m_pChild;

				m_pCurrentSolutionNode = m_pCurrentSolutionNode->m_pChild;

				return &pChild->m_SearchNode;
			}
		}
		return NULL;
	}


private:
	vector<Node *>	m_OpenList;					// Liste des nodes �examiner
	vector<Node *>	m_ClosedList;				// Liste des nodes d��examin�
	vector<Node *>	m_Successors;				// Liste des successeurs

	LechPathState	m_State;					// Etat du PathFinder
	u32				m_Steps;					// Nombre d'�apes d��trait�s

	Node			*m_pStart;					// Node de d�art
	Node			*m_pGoal;					// Node �atteindre
	Node			*m_pCurrentSolutionNode;	// Node pour parcourir la liste des nodes du chemin trouv�

	
	// Fonctions de lib�ation de la m�oire utilis� pour les nodes

	
	// Lib�er tous les nodes (un peu de m�age ca fait pas de mal)

	void FreeAllNodes()
	{
		for (vector<Node *>::iterator iterOpen = m_OpenList.begin(); iterOpen != m_OpenList.end(); iterOpen++)
		{
			Node *n = (*iterOpen);
			delete n;
		}
		m_OpenList.clear();

		for (vector<Node *>::iterator iterClosed = m_ClosedList.begin(); iterClosed != m_ClosedList.end(); iterClosed++)
		{
			Node *n = (*iterClosed);
			delete  n;
		}
		m_ClosedList.clear();
	}

	// Lib�er les nodes inutiles

	void FreeUnusedNodes()
	{
		for (vector<Node *>::iterator iterOpen = m_OpenList.begin(); iterOpen != m_OpenList.end(); iterOpen++)
		{
			Node *n = (*iterOpen);

			if ( !n->m_pChild )
			{
				delete n;
				n = NULL;
			}
		}
		m_OpenList.clear();

		for (vector<Node *>::iterator iterClosed = m_ClosedList.begin(); iterClosed != m_ClosedList.end(); iterClosed++)
		{
			Node *n = (*iterClosed);

			if ( !n->m_pChild )
			{
				delete n;
				n = NULL;
			}
		}
		m_ClosedList.clear();
	}
};


#endif // __LECHPATH_H
