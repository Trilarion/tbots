//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: LechBot.cpp
//	Author			: Aitonfrere	
//
//	Date			: 19/07/2003
//	Modification	:
//
//			03/08/2003	- Restructuration du code
//						- Gestion du pContext
//					 	- Gestion des nouveaux items (je sais je sais ... je suis �la bourre !)
//						- C'est un vrai chantier ce LechBot ! quel beau bordel hein ? ;)
//
//			21/07/2003	- Ajout d'un PathFinder-A*
//			20/07/2003	- Ajout dans l'API d'une fonction d'affichage en mode Debug
//			19/07/2003	- Naissance de LechBot dans le monde des T-Bots
//
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------

#include "LechPch.h"


////////////////////////////////////////////////////////////////////////////////
// Defines & Enumerations
////////////////////////////////////////////////////////////////////////////////

// Defines pour LECHBOT

#define LECHBOT_LONG_NAME			"LechBot The Lecheur"	// ... sans commentaire !
#define LECHBOT_SHORT_NAME			"LechBot"				// Ahhh.. l'inspiration, quand elle nous l�he ! :(

#define LECHBOT_VITALITY			5
#define LECHBOT_POWER				2
#define LECHBOT_SPEED				4
#define LECHBOT_VISION				4

#define LECHBOT_SLEEP()					pLechBot->m_BotAction.SetAction(KBA_NOTHING);

#define LECHBOT_MOVE(_Dir)				pLechBot->m_BotAction.SetAction(KBA_MOVE);\
										pLechBot->m_BotAction.SetMoveDir(_Dir);

#define LECHBOT_ATTACK(_Target)			pLechBot->m_BotAction.SetAction(KBA_ATTACK); \
										pLechBot->m_BotAction.SetAttackTarget(_Target);

#define LECHBOT_ATTACKLASER(_Target)	pLechBot->m_BotAction.SetAction(KBA_ATTACKLASER); \
										pLechBot->m_BotAction.SetAttackTarget(_Target);

// Activation des options d'affichage en mode Debug

//#define DISABLE_DEBUG_DISPLAY

//#define DEBUG_DISPLAY_FOGOFWAR
//#define DEBUG_DISPLAY_MEMORYMAP
//#define DEBUG_DISPLAY_MEMORYMAP_MAX
#define DEBUG_DISPLAY_PATH
#define DEBUG_DISPLAY_REGEN
#define DEBUG_DISPLAY_STATS


////////////////////////////////////////////////////////////////////////////////
// Global variables
////////////////////////////////////////////////////////////////////////////////

const KPt			g_MoveOffsets[4]		= { KPt(0,-1),		// KBMD_UP
												KPt(0,1),		// KBMD_DOWN
												KPt(-1,0),		// KBMD_LEFT
												KPt(1,0) };		// KBMD_RIGHT

const u8			g_DefaultItemsCost[]	= {	0,				// KIT_NONE
												1,				// KIT_BOT
												3,				// KIT_BONUSVISION
												2,				// KIT_BONUSPOWER
												3,				// KIT_BONUSSPEED
												4,				// KIT_BONUSJOKER
												0,				// KIT_REGENERATOR
												0,				// KIT_MEDIKIT
												0,				// KIT_BINOCULARS
												0,				// KIT_MINE
												0 };			// KIT_LASER

// [ ----- TODO : Attribuer les priorit� en fonction des items et de l'�at de LechBot
u8					g_CustomItemsCost[]		= {	0,				// KIT_NONE
												1,				// KIT_BOT
												3,				// KIT_BONUSVISION
												2,				// KIT_BONUSPOWER
												3,				// KIT_BONUSSPEED
												4,				// KIT_BONUSJOKER
												0,				// KIT_REGENERATOR
												0,				// KIT_MEDIKIT
												0,				// KIT_BINOCULARS
												0,				// KIT_MINE
												0 };			// KIT_LASER
// ------------------------------ ]


////////////////////////////////////////////////////////////////////////////////
// 								CLASS LechBot
////////////////////////////////////////////////////////////////////////////////
class LechBot
{
public :
	
	// LechBot information
	KBotInfo			m_BotInfo;
	KBotAction			m_BotAction;
	KBOTMOVEDIR			m_MoveDir;
	KBotActionInfo		*m_pBotActionInfo;

	// [ ----- TEMP : A garder si on peut pas faire autrement
	KPt					m_PreviousPos;
	s32					m_PreviousLife;
	// ------------------------------ ]

	// Memory information
	vector<KPt>			m_MemorizeRegen;
	vector<KPt>			m_MemorizeMines;

	// The Global Map

	LechMap				*m_pGlobalMap;

	// Target information

	KItemInfo			m_TargetInfo;
	bool				m_bTargetLocked;
	bool				m_bIsAttacked;

	// Path information

	vector<KPt>			m_Path;
	u32					m_PathProgress;

	// Debug information

	LechBotStats		m_DebugStats;


	LechBot()
	{
		m_MoveDir		= (KBOTMOVEDIR) (rand() % KBMD_COUNT);
		m_PreviousPos	= INVALID_POSITION;
		m_PreviousLife	= 0;
		m_MemorizeRegen.clear();
		m_MemorizeMines.clear();
		Reset();
	}

	~LechBot()		{ SafeDelete(m_pGlobalMap); }

	// Remise �z�o des infos de d�lacement
	void Reset()
	{
		m_Path.clear();
		m_PathProgress	= 0;
		m_TargetInfo.SetType(KIT_NONE);
		m_bTargetLocked	= false;
		m_bIsAttacked	= false;
	}

	// Trouver la direction �suivre pour atteindre une cible

	inline KBOTMOVEDIR	GetNewDirection(KPt _Start, KPt _Destination)
	{
		if ( _Destination.x < _Start.x )	return KBMD_LEFT;
		if ( _Destination.x > _Start.x )	return KBMD_RIGHT;
		if ( _Destination.y < _Start.y )	return KBMD_UP;
		return KBMD_DOWN;	// par d�aut
	}


	u32 hasInInventory(KITEMTYPE _Type)
	{
		u32 CountItem = 0;

		for (u32 i = 0; i < m_pBotActionInfo->m_nInventory; i++)
		{
			if (m_pBotActionInfo->m_pInventory[i].GetType() == _Type)
				CountItem++;
		}
		return CountItem;
	}

	// Mettre �jour la GlobalMap en fonction du champ de vision

	void UpdateGlobalMap()
	{
		CellType Type;

		for (s32 my = 0; my < m_pGlobalMap->m_MapHeight; my++)
			for (s32 mx = 0; mx < m_pGlobalMap->m_MapWidth; mx++) 
			{
				Type = ((KCell *)&m_pBotActionInfo->m_pMapCell[my * m_pGlobalMap->m_MapWidth + mx])->GetType();
				
				if ( Type != UNKNOWN_CELL_TYPE )
				{
					(*m_pGlobalMap)(mx,my)->m_Type = Type;

					if ( Type && !(*m_pGlobalMap)(mx,my)->m_Memory )
						(*m_pGlobalMap)(mx,my)->m_Memory = (u8)-1;
				}
				(*m_pGlobalMap)(mx,my)->m_Cost = ((*m_pGlobalMap)(mx,my)->m_Type != 0) * -1;
			}
	}

	// Trouver une direction al�toire pour ce tour

	KPt	FindRandomDirection()
	{
		KPt TargetPosition;
		u8	MinDirection = 0;
		u16	MinCostDirection = (u16)-1;

		// Chercher la direction la moins explor� et la moins coteuse (comme ca LechBot devrait pouvoir parcourir toute la Map)
		// LechBot aime bien d�ouvrir de nouveaux horizons :)
		for (u8 d = 0; d < 4; d++)
		{
			TargetPosition = m_pBotActionInfo->m_Pos;
			TargetPosition.x += g_MoveOffsets[(d + m_MoveDir) & 3].x;
			TargetPosition.y += g_MoveOffsets[(d + m_MoveDir) & 3].y;
			if ( ((*m_pGlobalMap)(TargetPosition.x,TargetPosition.y)->m_Memory + (*m_pGlobalMap)(TargetPosition.x,TargetPosition.y)->m_Cost) < MinCostDirection )
			{
				MinCostDirection = (*m_pGlobalMap)(TargetPosition.x,TargetPosition.y)->m_Memory + (*m_pGlobalMap)(TargetPosition.x,TargetPosition.y)->m_Cost;
				MinDirection = (d + m_MoveDir) & 3;
			}
		}

		KPt	Result;
		
		Result.x = m_pBotActionInfo->m_Pos.x + g_MoveOffsets[MinDirection].x;
		Result.y = m_pBotActionInfo->m_Pos.y + g_MoveOffsets[MinDirection].y;
		return Result;
	}

	// Rechercher l'item le plus proche

	KItemInfo FindNearestItem(KItemInfo* _pItems, u32 _nItems, KITEMTYPE _Type, bool _bAny)
	{
		KItemInfo	TargetInfo;
		float		Nearest = 10000.0f;
		float		Distance;

		TargetInfo.SetType(KIT_NONE);

		for (u32 i = 0; i < _nItems; i++)
		{
			// Oh! on a trouv�un item
			if ( _bAny || ( _pItems[i].GetType() == _Type) )
			{
				switch(_pItems[i].GetType())
				{
					case KIT_BOT :
						// LechBot ne se pr�ccupe pas des bots d��morts
						if ( !_pItems[i].IsAlive() ) 
							continue;

						// "Oh ?! Mais c'est moi !", dit LechBot en se voyant dans la liste ...
						if ( _pItems[i].GetPos() == m_pBotActionInfo->m_Pos ) 
							continue;

						// On a toujours dit �LechBot de ne pas s'approcher des autres bots quand ils trop dangereux
						if ( g_CustomItemsCost[KIT_BOT] >= PATH_MAX_COST ) 
							continue;

						break;
					default:
						break;
				}

				Distance = _pItems[i].GetPos().Distance(m_pBotActionInfo->m_Pos);

				// LechBot m�orise l'item le plus proche de lui
				if ( Distance < Nearest )
				{
					TargetInfo.SetPos(_pItems[i].GetPos());
					TargetInfo.SetAlive(_pItems[i].IsAlive());
					TargetInfo.SetType(_pItems[i].GetType());
					Nearest = Distance;
				}
			}
		}

		return TargetInfo;
	}

	// Evaluer le cot de chaque cellule de la GlobalMap (pour la recherche de chemin)

	void SetItemsCost(KItemInfo *_pItems, u32 _nItems)
	{
		u32 i;

		for (i = 0; i < _nItems; i++)
		{
			if ( (_pItems[i].GetType() == KIT_BOT) && (_pItems[i].GetPos() == m_pBotActionInfo->m_Pos) )
				continue;

			(*m_pGlobalMap)(_pItems[i].GetPos().x,_pItems[i].GetPos().y)->m_Cost = g_CustomItemsCost[_pItems[i].GetType()];

			if (_pItems[i].GetType() == KIT_BOT && !_pItems[i].IsAlive())
				(*m_pGlobalMap)(_pItems[i].GetPos().x,_pItems[i].GetPos().y)->m_Cost = 0;
		}

		// Condamne �tout jamais les cases sur lesquelles LechBot �pos�une mine (�consommer avec mod�ation!)
		for (i = 0; i < m_MemorizeMines.size(); i++)
			(*m_pGlobalMap)(m_MemorizeMines[i].x,m_MemorizeMines[i].y)->m_Cost = (8)-1;
	}

	// Trouver un objet qui sert �rien dans l'inventaire (bon qui sert le moins on va dire)

	KITEMTYPE GetMaxCostInventory()
	{
		KITEMTYPE Type = KIT_NONE;
		u8 MaxCost = 0;

		for (u32 i = 0; i < m_pBotActionInfo->m_nInventory; i++)
		{
			if (g_CustomItemsCost[m_pBotActionInfo->m_pInventory[i].GetType()] > MaxCost)
			{
				Type	= m_pBotActionInfo->m_pInventory[i].GetType();
				MaxCost	= g_CustomItemsCost[m_pBotActionInfo->m_pInventory[i].GetType()];
			}
		}

		return Type;
	}

	// M�oriser les Regen (pour que LechBot puisse y aller plus vite en cas de besoin) ... 
	// euh au cas o il aurait bobo je voulais dire

	void MemorizeRegenerators()
	{
		u32 i,r;
		KPt PosRegen;

		for (i = 0; i < m_pBotActionInfo->m_nItems; i++)
		{
			if (m_pBotActionInfo->m_pItems[i].GetType() == KIT_REGENERATOR)
			{
				PosRegen = m_pBotActionInfo->m_pItems[i].GetPos();
				for (r = 0; r < m_MemorizeRegen.size(); r++)
				{
					if (m_MemorizeRegen[r] == PosRegen)
						break;
				}

				if (r == m_MemorizeRegen.size())
					m_MemorizeRegen.push_back(PosRegen);
			}
		}
	}

	// Trouver le Regenerator le plus proche

	KPt FindNearestRegenerator()
	{
		KPt		PosRegen(-1,-1);
		float	Nearest = 10000.0f;
		float	Distance;

		for (u32 i = 0; i < m_MemorizeRegen.size(); i++)
		{
			Distance = m_MemorizeRegen[i].Distance(m_pBotActionInfo->m_Pos) + (*m_pGlobalMap)(m_MemorizeRegen[i].x,m_MemorizeRegen[i].y)->m_Cost*50;

			// LechBot m�orise le Regen le plus proche de lui
			if ( Distance < Nearest )
			{
				PosRegen = m_MemorizeRegen[i];
				Nearest = Distance;
			}
		}

		return PosRegen;
	}

	// Rechercher un chemin de "Start" �"Dest"

	LechPathState FindPath(KPt _Start, KPt _Dest)
	{
		LechPathFinder	PathFinder;
		LechPathState	SearchState;

		PathFinder.SetMap(m_pGlobalMap);

		// Point de d�art
		LechPathFinder::SearchNode NodeStart(_Start);

		// Point de ... arriv� !!
		LechPathFinder::SearchNode NodeGoal(_Dest);

		// Mettre �jour la fin du path (au cas o la cible bougerait .. hmm ca sent le bot adverse ca)
		if (m_Path.size())
		{
			NodeStart.m_Position = m_Path[m_Path.size()-1];
			m_Path.pop_back();
		}

		// Initialiser le PathFinder
		PathFinder.SetStartAndGoalNodes(NodeStart, NodeGoal);

		// Tant qu'on cherche on continue ! (wow c'est puissant ca !)
		do
		{
			SearchState = PathFinder.SearchStep();
		} while (SearchState == LPS_SEARCHING);
		
		// Et si on trouve bah on est content alors faut m�oriser tout ca
		if ( SearchState == LPS_SUCCEEDED )
		{
			LechPathFinder::SearchNode *pNode = PathFinder.GetSolutionStart();

			// Si c'est un nouveau path on saute la premi�e valeur (position actuelle)
			if (m_Path.empty() && pNode)
				pNode = PathFinder.GetSolutionNext();

			// Empile tous les nodes dans le chemin
			while (pNode)
			{
				m_Path.push_back(pNode->m_Position);
				pNode = PathFinder.GetSolutionNext();
			}

			PathFinder.FreeSolutionNodes();
		}

		return SearchState;
	}

	// Bon! c'est au LechBot de choisir ca cible ...
	void SelectTargetType()
	{
		// On en prend une au pif, la plus proche tant qu'�faire
		KItemInfo TargetInfo = FindNearestItem(m_pBotActionInfo->m_pItems, m_pBotActionInfo->m_nItems, KIT_NONE, true);

		// S'il y en a une
		if (TargetInfo.GetType() != KIT_NONE)
		{
			switch(TargetInfo.GetType())
			{
				case KIT_BINOCULARS :
						m_TargetInfo.SetType(TargetInfo.GetType());
						break;
		
				case KIT_MEDIKIT :
				case KIT_REGENERATOR :
					if ( m_pBotActionInfo->m_Life < GETLIFE(LECHBOT_VITALITY) )
					{
						m_TargetInfo.SetType(TargetInfo.GetType());
						break;
					}

				default :
					m_TargetInfo.SetType(KIT_NONE);
					break;
			}
		}

		if (m_TargetInfo.GetType() != KIT_NONE)
			return;

		// Si LechBot commence �faiblir
		if ( (m_pBotActionInfo->m_Life < (GETLIFE(LECHBOT_VITALITY)) / 4))
		{
			// Si LechBot a d��apercu un Regenerator
			if (m_MemorizeRegen.size())
			{
				// Il choisit d'aller y faire un tour
				m_TargetInfo.SetType(KIT_REGENERATOR);
			}
			else
			{
				// Il choisit d'aller chercher un MediKit
				m_TargetInfo.SetType(KIT_MEDIKIT);
			}

			// LechBot n'a pas envie de faire de mauvaises rencontres ...
			g_CustomItemsCost[KIT_BOT] = PATH_MAX_COST;
		}
		// Tant que son sac n'est pas plein
		else if (m_pBotActionInfo->m_nInventory < 5 && !m_bIsAttacked)
		{
			// Quand son sac est vide						LechBot aime bien le remplir avec des objets ou des armes :)

			// Plutt des armes :))
			m_TargetInfo.SetType(KIT_LASER);
		}
		else
		{
			m_TargetInfo.SetType(KIT_BOT);
			g_CustomItemsCost[KIT_BOT] = g_DefaultItemsCost[KIT_BOT];
		}
	}
};


////////////////////////////////////////////////////////////////////////////////
// Prototypes
////////////////////////////////////////////////////////////////////////////////
char	*FormatString(const char *_Format, ...);


//==============================================================================
//
//								  T-Bots DllMain
//
//==============================================================================

////////////////////////////////////////////////////////////////////////////////
// DllMain
//
////////////////////////////////////////////////////////////////////////////////
#ifdef _WIN32
BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call,  LPVOID lpReserved)
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}
#endif // _WIN32

//==============================================================================
//
//							  T-Bots API Functions
//
//==============================================================================

////////////////////////////////////////////////////////////////////////////////
// GetpBotName
//
////////////////////////////////////////////////////////////////////////////////
LECHBOT_API char* GetpBotName()
{
	// Nom long du Robot
	return LECHBOT_LONG_NAME;
}

////////////////////////////////////////////////////////////////////////////////
// GetpInfos
//
////////////////////////////////////////////////////////////////////////////////
LECHBOT_API KBotInfo* GetpInfos(void* pContext)
{
	LechBot *pLechBot = (LechBot *)pContext;

	if (!pContext) return NULL;

	// Nom court de LechBot
	pLechBot->m_BotInfo.SetpBotName(LECHBOT_SHORT_NAME);

	// Les caract�istiques de LechBot
	pLechBot->m_BotInfo.SetPoints(LECHBOT_VITALITY, LECHBOT_POWER, LECHBOT_SPEED, LECHBOT_VISION);
	
	return &pLechBot->m_BotInfo;
}

////////////////////////////////////////////////////////////////////////////////
// StartGame
//
////////////////////////////////////////////////////////////////////////////////
LECHBOT_API void *StartGame(s32 MapWidth, s32 MapHeight)
{
	LechBot *pLechBot = NULL;

	pLechBot					= new LechBot;
	pLechBot->m_pGlobalMap		= new LechMap(MapWidth,MapHeight);

	srand((unsigned int)pLechBot->m_DebugStats.GetTime());

	return pLechBot;
}

////////////////////////////////////////////////////////////////////////////////
// EndGame
//
////////////////////////////////////////////////////////////////////////////////
LECHBOT_API void EndGame(void* pContext)
{
	LechBot *pLechBot = (LechBot *)pContext;

	SafeDelete(pLechBot);
}

////////////////////////////////////////////////////////////////////////////////
// DebugDisplay
//
////////////////////////////////////////////////////////////////////////////////
/*LECHBOT_API void DebugDisplay(KRender *pRender,void* pContext)
{
	LechBot *pLechBot = (LechBot *)pContext;

	if (!pContext) return;

#ifndef DISABLE_DEBUG_DISPLAY

	// Afficher la "m�oire de passage" dans chaque cellule de la GlobalMap
	// Ca ressemble �de la bave, mais ce n'est pas de la bave !! LechBot est propre !

	#ifdef DEBUG_DISPLAY_MEMORYMAP
		{
			u8 MemoryCell;
			for (s32 my = 0; my < pLechBot->m_pGlobalMap->m_MapHeight; my++)
			{
				for (s32 mx = 0; mx < pLechBot->m_pGlobalMap->m_MapWidth; mx++) 
				{
					if ( MemoryCell = (*pLechBot->m_pGlobalMap)(mx,my)->m_Memory )
					{
		#ifndef DEBUG_DISPLAY_MEMORYMAP_MAX
						if (MemoryCell != 255)
		#endif
						pRender->DrawQuad(	SCREEN_OFFSETX + mx*CELL_SIZE, my*CELL_SIZE,
											CELL_SIZE, CELL_SIZE, 0,
											KRGBA(0,0,255,(MemoryCell*16)) );
					}
				}
			}
		}
	#endif	// DEBUG_DISPLAY_MEMORYMAP

	// Afficher le Path courant (�partir de la position de LechBot)

	#ifdef DEBUG_DISPLAY_PATH
		{
			KPt Position;

			for (s32 i = pLechBot->m_PathProgress; i < pLechBot->m_Path.size(); i++)
			{
				Position = pLechBot->m_Path[i];

				pRender->DrawQuad(	SCREEN_OFFSETX + Position.x*CELL_SIZE, Position.y*CELL_SIZE,
									CELL_SIZE, CELL_SIZE, 0,
									KRGBA(0,255,0,32) );
			}
		}
	#endif	// DEBUG_DISPLAY_PATH

	// Afficher le Fog Of War de LechBot

	#ifdef DEBUG_DISPLAY_FOGOFWAR
		for (s32 my = 0; my < pLechBot->m_pGlobalMap->m_MapHeight; my++)
		{
			for (s32 mx = 0; mx < pLechBot->m_pGlobalMap->m_MapWidth; mx++) 
			{
				if (pLechBot->m_pGlobalMap[my][mx]->Type == UNKNOWN_CELL_TYPE)
				{
					pRender->DrawQuad(	SCREEN_OFFSETX + mx*CELL_SIZE, my*CELL_SIZE,
										CELL_SIZE, CELL_SIZE, 0,
										KRGBA(0,0,0,255) );
				}
			}
		}
	#endif	// DEBUG_DISPLAY_FOGOFWAR

	#ifdef DEBUG_DISPLAY_REGEN
		{
			KPt Position;

			for (s32 r = 0; r < pLechBot->m_MemorizeRegen.size(); r++)
			{
				Position = pLechBot->m_MemorizeRegen[r];

				pRender->DrawQuad(	SCREEN_OFFSETX + Position.x*CELL_SIZE, Position.y*CELL_SIZE,
									CELL_SIZE, CELL_SIZE, 0,
									KRGBA(255,0,0,128) );
			}
		}
	#endif // DEBUG_DISPLAY_REGEN

	// Afficher quelques stats

	#ifdef DEBUG_DISPLAY_STATS
		#define STATS_OFFSETX	5
		#define STATS_OFFSETY	400
		#define FONT_SIZE		10
		#define FONT_VSPACE		15

		pRender->DrawText(STATS_OFFSETX,STATS_OFFSETY + 0*FONT_VSPACE,FormatString("Turn duration AVG : %d",pLechBot->m_DebugStats.GetTurnDurationAVG()),FONT_SIZE);
		pRender->DrawText(STATS_OFFSETX,STATS_OFFSETY + 1*FONT_VSPACE,FormatString("Turn count        : %d",pLechBot->m_DebugStats.m_TurnCount),FONT_SIZE);
		pRender->DrawText(STATS_OFFSETX,STATS_OFFSETY + 2*FONT_VSPACE,FormatString("Path length       : %d",pLechBot->m_DebugStats.m_PathLength),FONT_SIZE);
		pRender->DrawText(STATS_OFFSETX,STATS_OFFSETY + 3*FONT_VSPACE,FormatString("Current MoveDir   : %d",pLechBot->m_MoveDir),FONT_SIZE);
		pRender->DrawText(STATS_OFFSETX,STATS_OFFSETY + 4*FONT_VSPACE,FormatString("MemRegen count    : %d",pLechBot->m_MemorizeRegen.size()),FONT_SIZE);
		pRender->DrawText(STATS_OFFSETX,STATS_OFFSETY + 5*FONT_VSPACE,FormatString("MemMines count    : %d",pLechBot->m_MemorizeMines.size()),FONT_SIZE);
	#endif	// DEBUG_DISPLAY_STATS

#endif	// DISABLE_DEBUG_DISPLAY
}
*/
////////////////////////////////////////////////////////////////////////////////
// GetpBotAction
//
////////////////////////////////////////////////////////////////////////////////
LECHBOT_API KBotAction *GetpBotAction(KBotActionInfo* pBotActionInfo, void* pContext)
{
	LechBot *pLechBot = (LechBot *)pContext;
	bool	bRegenerating = false;
	
	if (!pContext) return NULL;
	pLechBot->m_pBotActionInfo = pBotActionInfo;

	s64 ThisTurnDuration = pLechBot->m_DebugStats.GetTime();		// [DEBUG INFO]
	pLechBot->m_DebugStats.m_bRandomTarget = false;					// [DEBUG INFO]


	// M�oriser les parties manquante de la GlobalMap
	pLechBot->UpdateGlobalMap();
	pLechBot->MemorizeRegenerators();
	pLechBot->SetItemsCost(pBotActionInfo->m_pItems,pBotActionInfo->m_nItems);

	// Par d�aut LechBot ne fait rien car il aime se pr�asser au soleil
	pLechBot->m_BotAction.SetAction(KBA_NOTHING);

	// Mais ce n'est pas un feignant pour autant il retient les chemins qu'il a d��travers�!
	if (pBotActionInfo->m_Pos != pLechBot->m_PreviousPos)
		(*pLechBot->m_pGlobalMap)(pBotActionInfo->m_Pos.x,pBotActionInfo->m_Pos.y)->m_Memory++;

	// Attention ca commence �chauffer ...
	if ( pBotActionInfo->m_Life < GETLIFE(LECHBOT_VITALITY) && pLechBot->m_TargetInfo.GetType() == KIT_REGENERATOR )
	{
		KPt TargetPos = pLechBot->FindNearestRegenerator();
		if (TargetPos == pBotActionInfo->m_Pos)
			bRegenerating = true;
			
		else if (pBotActionInfo->m_Life != (u32)pLechBot->m_PreviousLife)
		{
			g_CustomItemsCost[KIT_BOT] = g_DefaultItemsCost[KIT_BOT];
			pLechBot->Reset();
			pLechBot->m_bIsAttacked = true;
		}
	}
	else if (pBotActionInfo->m_Life != (u32)pLechBot->m_PreviousLife)
	{
		g_CustomItemsCost[KIT_BOT] = g_DefaultItemsCost[KIT_BOT];
		pLechBot->Reset();
		pLechBot->m_bIsAttacked = true;
	}

	// Si le chemin est trop long alors ca suffit comme ca, LechBot tient �se m�ager !
	if ( pLechBot->m_Path.size() )
	{
		if (pLechBot->m_bIsAttacked)
		{
			pLechBot->Reset();
		}
		else if (pLechBot->m_PathProgress == pLechBot->m_Path.size())
		{
			if (!bRegenerating)
				pLechBot->Reset();
			
			pLechBot->m_BotAction.SetAction(KBA_NOTHING);
		}
		else if (pLechBot->m_Path.size() > PATH_LENGTH_THRESHOLD)
		{
			pLechBot->Reset();
			pLechBot->m_BotAction.SetAction(KBA_NOTHING);
		}
	}

	// D�ose une mine si LechBot en a
	if (pLechBot->hasInInventory(KIT_MINE))
	{
		pLechBot->m_MemorizeMines.push_back(KPt(pBotActionInfo->m_Pos));
		pLechBot->m_BotAction.SetAction(KBA_DROP);
		pLechBot->m_BotAction.SetDropItemType(KIT_MINE);
	}

	if (bRegenerating)
	{
		// LechBot se refait une sant�:) mais ... on sait jamais ce qu'il peut se passer aux alentours
		// Alors LechBot n'aime pas se faire taper dessus pendant qu'il se soigne

		g_CustomItemsCost[KIT_BOT] = g_DefaultItemsCost[KIT_BOT];
		
		KItemInfo TargetInfo = pLechBot->FindNearestItem(pBotActionInfo->m_pItems,pBotActionInfo->m_nItems, KIT_BOT, false);

		if ( (TargetInfo.GetType() == KIT_BOT) &&
			 (TargetInfo.GetPos().Distance(pBotActionInfo->m_Pos) <= 1.01f) && TargetInfo.IsAlive() )
		{
			// Allez tape le !!!

			if (pLechBot->hasInInventory(KIT_LASER))
			{
				LECHBOT_ATTACKLASER((KBOTATTACKTARGET) pLechBot->GetNewDirection(pBotActionInfo->m_Pos,TargetInfo.GetPos()));
			}
			else
			{
				LECHBOT_ATTACK((KBOTATTACKTARGET) pLechBot->GetNewDirection(pBotActionInfo->m_Pos,TargetInfo.GetPos()));
			}
		}
	}
	else if (pBotActionInfo->m_nInventory == 5 && pLechBot->hasInInventory(KIT_LASER) < 2)
	{
		pLechBot->m_BotAction.SetAction(KBA_DROP);
		pLechBot->m_BotAction.SetDropItemType(pLechBot->GetMaxCostInventory());
	}
	else if (pLechBot->m_BotAction.GetAction() == KBA_NOTHING && !bRegenerating)
	{
		// Si LechBot n'a rien pr�u aujourd'hui
		if (!pLechBot->m_bTargetLocked)
		{
			pLechBot->SelectTargetType();

			// LechBot a peut-�re trouv�une cible  ?
			switch(pLechBot->m_TargetInfo.GetType())
			{
				case KIT_NONE :
					break;

				case KIT_REGENERATOR :
					// Alors LechBot est tr� content, il va pouvoir s'occuper :))
					pLechBot->m_bTargetLocked = true;

					// Maintenant il va lui falloir trouver un chemin jusqu'�la cible
					pLechBot->m_bTargetLocked = (pLechBot->FindPath(pBotActionInfo->m_Pos,
																	pLechBot->FindNearestRegenerator()) == LPS_SUCCEEDED);
					break;

				case KIT_BINOCULARS :
				case KIT_MEDIKIT :
				case KIT_LASER :
				case KIT_BOT	 :
					pLechBot->m_TargetInfo = pLechBot->FindNearestItem(pBotActionInfo->m_pItems, pBotActionInfo->m_nItems, pLechBot->m_TargetInfo.GetType(), false);
					if (pLechBot->m_TargetInfo.GetType() == KIT_NONE)
						break;

				default :
					// Alors LechBot est tr� content, il va pouvoir s'occuper :))
					pLechBot->m_bTargetLocked = true;

					// Maintenant il va lui falloir trouver un chemin jusqu'�la cible
					pLechBot->m_bTargetLocked = (pLechBot->FindPath(pBotActionInfo->m_Pos,
																	pLechBot->m_TargetInfo.GetPos()) == LPS_SUCCEEDED);
					break;
			}
		}

		if ( pLechBot->m_TargetInfo.GetType() == KIT_BOT &&
			 pLechBot->m_TargetInfo.GetPos().Distance(pBotActionInfo->m_Pos) <= 2.01f &&
			 pLechBot->m_TargetInfo.IsAlive() )
		{
			// LechBot s'arr�e de parcourir le chemin actuel
			pLechBot->m_bTargetLocked		= false;
			
			// Et il attaque !!! (ou��)

			if (pLechBot->hasInInventory(KIT_LASER))
			{
				LECHBOT_ATTACKLASER((KBOTATTACKTARGET) pLechBot->GetNewDirection(pBotActionInfo->m_Pos,pLechBot->m_TargetInfo.GetPos()));
			}
			else
			{
				LECHBOT_ATTACK((KBOTATTACKTARGET) pLechBot->GetNewDirection(pBotActionInfo->m_Pos,pLechBot->m_TargetInfo.GetPos()));
			}
		}
		// Si LechBot a d��m�oris�un chemin alors il continue
		else if ( pLechBot->m_PathProgress < pLechBot->m_Path.size() )
		{
			// Tiens j'ai une bonne nouvelle mon cher LechBot :
			// "Tu n'es pas encore arriv� La route est encore longue ... Allez on se bouge !"
			pLechBot->m_MoveDir = pLechBot->GetNewDirection(pBotActionInfo->m_Pos,pLechBot->m_Path[pLechBot->m_PathProgress++]);

			LECHBOT_MOVE(pLechBot->m_MoveDir);
		}
		else
		{
			// D�id�ent LechBot a bel et bien d�id�de ne rien faire !
			// Bah alors on va bouger un petit peu (mais alors un petit peu seulement)

			KPt TargetPos;

			pLechBot->Reset();
			pLechBot->m_DebugStats.m_bRandomTarget	= true;									// [DEBUG INFO]
			TargetPos								= pLechBot->FindRandomDirection();
			pLechBot->m_MoveDir						= pLechBot->GetNewDirection(pBotActionInfo->m_Pos,TargetPos);

			LECHBOT_MOVE(pLechBot->m_MoveDir);
		}
	}

	// M�orise l'�at courant de LechBot (pour le prochain tour)
	pLechBot->m_PreviousLife					= pBotActionInfo->m_Life;
	pLechBot->m_PreviousPos						= pBotActionInfo->m_Pos;

	// Quelques infos pour le mode Debug
	pLechBot->m_DebugStats.m_TurnDurationSum	+= pLechBot->m_DebugStats.GetTime() - ThisTurnDuration;	// [DEBUG INFO]
	pLechBot->m_DebugStats.m_PathLength			= pLechBot->m_Path.size();								// [DEBUG INFO]
	pLechBot->m_DebugStats.m_TurnCount++;																// [DEBUG INFO]

	return &pLechBot->m_BotAction;
}


//==============================================================================
//
//								Misc. Functions
//
//==============================================================================

////////////////////////////////////////////////////////////////////////////////
// FormatString
//
////////////////////////////////////////////////////////////////////////////////
char *FormatString(const char *_Format, ...)
{
	static char TempString[256];
	va_list args;
	va_start(args,_Format);

#ifdef _WIN32
	_vsnprintf(TempString,sizeof(TempString),_Format,args);
#endif // _WIN32
#ifdef _LINUX
	vsnprintf(TempString,sizeof(TempString),_Format,args);
#endif // _LINUX
	return ((char *)TempString);
}
