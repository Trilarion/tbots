//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: PrenMesBot.cpp
//	Author			: - Aitonfrere	
//					  - Dino			dino@zythum-project.com
//
//	Date			: 22/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------

#include <vector>

#include "PrenMesBot.h"


////////////////////////////////////////////////////////////////////////////////
// Defines & Enumerations
////////////////////////////////////////////////////////////////////////////////

#define PRENMESBOT_LONG_NAME			"PrenMesBot The Debugging Bot"
#define PRENMESBOT_SHORT_NAME			"PrenMesBot"


////////////////////////////////////////////////////////////////////////////////
// Global variables
////////////////////////////////////////////////////////////////////////////////

// PrenMesBot local information

KBotInfo			g_BotInfo;
KBotAction			g_BotAction;



////////////////////////////////////////////////////////////////////////////////
// DllMain
//
////////////////////////////////////////////////////////////////////////////////
#ifdef _WIN32
BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call,  LPVOID lpReserved)
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}
#endif // _WIN32

//==============================================================================
//
//							  T-Bots API Functions
//
//==============================================================================

////////////////////////////////////////////////////////////////////////////////
// GetpBotName
//
////////////////////////////////////////////////////////////////////////////////
PRENMESBOT_API char* GetpBotName()
{
	// Nom long du Robot
	return PRENMESBOT_LONG_NAME;
}

////////////////////////////////////////////////////////////////////////////////
// GetpInfos
//
////////////////////////////////////////////////////////////////////////////////
PRENMESBOT_API KBotInfo* GetpInfos( void* pContext )
{
	// Nom court du Robot
	g_BotInfo.SetpBotName(PRENMESBOT_SHORT_NAME);

	return &g_BotInfo;
}

////////////////////////////////////////////////////////////////////////////////
// StartGame
//
////////////////////////////////////////////////////////////////////////////////
PRENMESBOT_API void* StartGame(s32 MapWidth, s32 MapHeight)
{
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////
// EndGame
//
////////////////////////////////////////////////////////////////////////////////
PRENMESBOT_API void EndGame( void* pContext )
{
}

////////////////////////////////////////////////////////////////////////////////
// DebugDisplay
//
////////////////////////////////////////////////////////////////////////////////
/*PRENMESBOT_API void DebugDisplay(KRender *pRender,  void* pContext )
{
}
*/
////////////////////////////////////////////////////////////////////////////////
// GetpBotAction
//
////////////////////////////////////////////////////////////////////////////////
#define KEYPRESSED(_key)		( GetAsyncKeyState(_key) & 0x8000 )

PRENMESBOT_API KBotAction* GetpBotAction( KBotActionInfo* pBotActionInfo, void* pContext )
{
	KItemInfo	*pItemInfo	= NULL;

	g_BotAction.SetAction(KBA_NOTHING);

	////////////////////////////////////////////////////////////////////////////////
	//								BotAction KBA_MOVE
	////////////////////////////////////////////////////////////////////////////////

	// D�lacement vers la gauche
#ifdef _WIN32
	bool		isMoving	 = false;
	
	if ( KEYPRESSED(VK_LEFT) )
	{
		isMoving = true;
		g_BotAction.SetAction(KBA_MOVE);
		g_BotAction.SetMoveDir(KBMD_LEFT);
	}

	// D�lacement vers la droite

	else if ( KEYPRESSED(VK_RIGHT) )
	{
		isMoving = true;
		g_BotAction.SetAction(KBA_MOVE);
		g_BotAction.SetMoveDir(KBMD_RIGHT);
	}

	// D�lacement vers le haut

	else if ( KEYPRESSED(VK_UP) )
	{
		isMoving = true;
		g_BotAction.SetAction(KBA_MOVE);
		g_BotAction.SetMoveDir(KBMD_UP);
	}

	// D�lacement vers le bas

	else if ( KEYPRESSED(VK_DOWN) )
	{
		isMoving = true;
		g_BotAction.SetAction(KBA_MOVE);
		g_BotAction.SetMoveDir(KBMD_DOWN);
	}

	////////////////////////////////////////////////////////////////////////////////
	//					BotAction KBA_ATTACK & KBA_ATTACKLASER
	////////////////////////////////////////////////////////////////////////////////

	if ( isMoving )
	{
		// Attaque dans la direction sp�ifi�

		if ( KEYPRESSED(VK_LCONTROL) )
		{
			g_BotAction.SetAction(KBA_ATTACK);
			g_BotAction.SetAttackTarget( (KBOTATTACKTARGET) g_BotAction.GetMoveDir() );
		}

		// Attaque au laser dans la direction sp�ifi�

		else if ( KEYPRESSED(VK_LSHIFT) )
		{
			g_BotAction.SetAction(KBA_ATTACKLASER);
			g_BotAction.SetAttackTarget( (KBOTATTACKTARGET) g_BotAction.GetMoveDir() );
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//								BotAction KBA_DROP
	////////////////////////////////////////////////////////////////////////////////
		 if ( KEYPRESSED('1') || KEYPRESSED(VK_NUMPAD1) )	{ pItemInfo = (pBotActionInfo->m_nInventory > 0) ? &pBotActionInfo->m_pInventory[0] : NULL;	}
	else if ( KEYPRESSED('2') || KEYPRESSED(VK_NUMPAD2) )	{ pItemInfo = (pBotActionInfo->m_nInventory > 1) ? &pBotActionInfo->m_pInventory[1] : NULL;	}
	else if ( KEYPRESSED('3') || KEYPRESSED(VK_NUMPAD3) )	{ pItemInfo = (pBotActionInfo->m_nInventory > 2) ? &pBotActionInfo->m_pInventory[2] : NULL;	}
	else if ( KEYPRESSED('4') || KEYPRESSED(VK_NUMPAD4) )	{ pItemInfo = (pBotActionInfo->m_nInventory > 3) ? &pBotActionInfo->m_pInventory[3] : NULL;	}
	else if ( KEYPRESSED('5') || KEYPRESSED(VK_NUMPAD5) )	{ pItemInfo = (pBotActionInfo->m_nInventory > 4) ? &pBotActionInfo->m_pInventory[4] : NULL;	}
#endif // _WIN32
	if (pItemInfo )
	{
		g_BotAction.SetAction(KBA_DROP);
		g_BotAction.SetDropItemType(pItemInfo ->GetType());
	}

	return &g_BotAction;
}

