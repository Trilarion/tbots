//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 T-Bots Team
//
//	File			: Pathfinder.h
//	Author			: - Macload
//	Date			: 19/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the LECHBOT_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// LECHBOT_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.

#include <vector>
#include "BotDefs.h"
#include "Level.h"
#include "Bot.h"

#ifndef	__PATHFINDER_H__
#define	__PATHFINDER_H__

typedef struct _sCell
{
    int   iParentID ;
    int   iId ;
    int   iPosX ;
    int   iPosY ;
    float fG ;
    float fH ;
    float fResult ;
    bool  bWall ;
} sCell;

class CPathFinder
{
 public :
    CPathFinder(KCell* pMapCell,int iMapWidth,int iMapHeight,int iStartPosX,int iStartPosY,int iEndPosX,int iEndPosY) ;
    ~CPathFinder() ;

    bool ComputePath() ;
    int  GetNextCell() ;
    sCell GetCell(int iCellId) ;

    vector<int> m_FinalCellsList ;
    int         m_iEndPosX ;
    int         m_iEndPosY ;

 protected :
     
     sCell *m_pMapCell ;
     int m_iMapWidth ;
     int m_iMapHeight ;
     int m_iStartPosX ;
     int m_iStartPosY ;
     
     int m_iCount ;

     vector<int> m_OpenList ;
     vector<int> m_CloseList ;
     
     int  GetLowestSquare(void) ;
     void AddNearestSquaresToOpenList(int iParentSquareId,int iNewCell) ;
     bool IsInOpenList(int iCellId) ;
     void ChangeToCloseList(int iCellId) ;
     void CalculCoefficient(int iCellId,float fGParent) ;

};

#endif
