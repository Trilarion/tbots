//--------------------------------------------------------------------------------------------
// 
//	Copyright (C) 2003 Ruffi
//
//	File			: R_Bot.h
//	Author			: - Ruffi
//
//	Date			: 20/07/2003
//	Modification	:
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation (version 2 of the License)
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//--------------------------------------------------------------------------------------------
#ifdef _WIN32
#ifdef R_BOT_EXPORTS
#define R_BOT_API __declspec(dllexport)
#else
#define R_BOT_API __declspec(dllimport)
#endif
#endif // _WIN32

#ifdef _LINUX
#define R_BOT_API
#endif // _LINUX

#include "BotDefs.h"
#include "Level.h"
#include "Bot.h"

//--------------------------------------------------------------------------------------------
extern "C" R_BOT_API char* GetpBotName();
extern "C" R_BOT_API KBotInfo* GetpInfos( void* pContext );
extern "C" R_BOT_API void* StartGame( s32 MapWidth, s32 MapHeight );
extern "C" R_BOT_API void EndGame( void* pContext );
extern "C" R_BOT_API KBotAction* GetpBotAction( KBotActionInfo* pBotActionInfo, void* pContext );
//extern "C" R_BOT_API void DebugDisplay( KRender *pRender, void* pContext );
